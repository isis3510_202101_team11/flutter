import 'package:flutter/material.dart';
import 'package:minihomelab/components/sequence_timer.dart';
import '../Constanse.dart';

class SequenceArguments {
  final int level;
  final int number;
  final String name;
  final int seconds;

  SequenceArguments({
    @required this.level,
    @required this.number,
    @required this.name,
    @required this.seconds,
  });
}

class SequenceScreen extends StatelessWidget {
  //static const String routeName = '/sequence_screen';

  static const String id = 'sequence_screen';

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context).settings.arguments as SequenceArguments;

    return Scaffold(
        appBar: AppBar(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Text(
                  "Secuencia ${args.number}",
                  style: TextStyle(
                      color: DarkPurple,
                      fontWeight: FontWeight.bold,
                      fontSize: 40),
                ),
                Text(
                  "${args.name}",
                  style:
                      TextStyle(color: kligtgrey, fontWeight: FontWeight.bold),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.center,
            ),
            SequenceTimer(
                seconds: args.seconds, number: args.number, level: args.level)
          ],
        ));
  }
}

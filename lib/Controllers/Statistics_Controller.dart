import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/models/Statistics_Model.dart';

class StatisticsController {
  final String usageURL = urlLocal + "profiles/last/";
  final String exerciseURL = urlLocal + "sessions/exercises/";

  StatisticsController._privateConstructor();

  static final StatisticsController _instance =
      StatisticsController._privateConstructor();

  static StatisticsController get instance => _instance;

  Future<Statistics> GetAllStatistics(String userId) async {
    final ExerciseStatistics exercise = await getExercisesStatistics(userId);
    final UsageStatistics usage = await getUsageStatistics(userId);
    return Statistics(exercise: exercise, usage: usage);
  }

  Future<ExerciseStatistics> getExercisesStatistics(String userId) async {
    final res = await http.get(Uri.parse(exerciseURL + userId));
    try {
      return ExerciseStatistics.fromJson(json.decode(res.body));
    } catch (e) {
      return ExerciseStatistics(numCompleted: 0, numNotCompleted: 0);
    }
  }

  Future<UsageStatistics> getUsageStatistics(String userId) async {
    final res = await http.get(Uri.parse(usageURL + userId));
    return UsageStatistics.fromJson(json.decode(res.body));
  }
}

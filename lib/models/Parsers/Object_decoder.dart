//Clase que coge el Future y lo transforma
import 'dart:convert';

import 'Json_parser.dart';

//funcion que decodifica lo que llega de la rta en Future
//Le estoy metiendo la función a la clase abstracta JsonParser
mixin ObjectDecoder<T> on JsonParser<T> {
  //Convierto el json que le entra a un objeto dinámica
  Map<String, dynamic> decodeJsonObject(String json) =>
      jsonDecode(json) as Map<String, dynamic>;
}
//Ahora en caso de que me llegue una lista del back
mixin ListDecoder<T> on JsonParser<T> {
  //Convierto el json que le entra a un objeto dinámica
  List<dynamic> decodeJsonObject(String json) =>
      jsonDecode(json) as List<dynamic>;
}

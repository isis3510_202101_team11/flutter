//Obtiene un API

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/models/Parsers/Json_parser.dart';

//Le llega un API es obligatorio
class RequestREST {
  const RequestREST({
    @required this.api,
    this.data,
  });

  // El endpoint
  final String api;

  //Es la info que enviaría si es un POST o un PUT
  final Map<String, dynamic> data;

  // HTTP dio Client
  // Utilizo Dio porque puedo establecer un URL base, los timeout y puedo definir los header más amigables
  static final _client = Dio(
    BaseOptions(
      //Cambia la URL dependiendo de donde la corro
      //  || IOS: http://localhost:8081/   || https://mini-home-lab.herokuapp.com
      baseUrl: urlLocal,
      connectTimeout: 9000,
      receiveTimeout: 9000,
    ),
  );

  /// * Generic GET ALL
  Future<T> executeGet<T>(JsonParser<T> parser) async {
    final response = await _client.get<String>(api);
    print(api);
    return parser.parseFromJson(response.data);
  }

  /// * Generic GET ONE
  Future<T> executeGetOne<T>(JsonParser<T> parser) async {
    final response = await _client.get<String>(api);
    return parser.parseFromJson(response.data);
  }

  /// * Generic PUT
  Future<T> executePut<T>(JsonParser<T> parser, FormData formData) async {
    final response = await _client.put<String>(
      api,
      data: data ?? formData,
    );

    return parser.parseFromJson(response.data);
  }

  /// * Generic POST
  Future<T> executePost<T>(JsonParser<T> parser) async {
    final response = await _client.post<String>(
      api,
      data: data,
    );
    return parser.parseFromJson(response.data);
  }

  /// * Generic DELETE
  Future<T> executeDelete<T>(JsonParser<T> parser) async {
    final response = await _client.delete<String>(api);
    return parser.parseFromJson('{}'); // json.encode(resModified)
  }
}

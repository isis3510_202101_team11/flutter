import 'dart:io';
import 'package:flutter/material.dart';
import 'package:minihomelab/Components/Resusable_Button.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/screens/Home_Page.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:minihomelab/screens/Welcome_page.dart';
import 'package:minihomelab/Components/Reusable_Card.dart';
import 'package:minihomelab/Components/PopUpMenu.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class ProfileCard extends StatefulWidget {
  static const String id = 'profile_screen';

  @override
  _ProfileCardState createState() => _ProfileCardState();
}

class _ProfileCardState extends State<ProfileCard> {
  File photo;
  bool showProgress = false;
  String PhotUrl;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    FirebaseAnalytics().logEvent(name: 'profile_event', parameters: null);
    setState(() {
      PhotUrl = LoginController.currentUser.ProfilepickUrl;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.pushNamed(context, HomePage.id),
        ),
        title: Text("Mini-Home Lab"),
        centerTitle: true,
      ),
      backgroundColor: KbackGroundColor,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Hero(
              tag: "ProfilePic",
              child: CircleAvatar(
                radius: 80.0,
                backgroundImage:
                    LoginController.currentUser.ProfilepickUrl == ""
                        ? AssetImage("images/defaultprofile.jpg")
                        : CachedNetworkImageProvider(
                            PhotUrl,
                          ),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: RawMaterialButton(
                    fillColor: kPrimaryDarkGrey,
                    shape: CircleBorder(),
                    constraints:
                        BoxConstraints(minHeight: 50.0, minWidth: 50.0),
                    child: Icon(
                      Icons.add_to_photos_outlined,
                      color: KbackGroundColor,
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return StatefulBuilder(
                            builder: (context, setState) {
                              return ModalProgressHUD(
                                inAsyncCall: showProgress,
                                child: ReusableCard(
                                  children: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: RawMaterialButton(
                                          fillColor: kPrimaryDarkGrey,
                                          shape: CircleBorder(),
                                          constraints: BoxConstraints(
                                              minHeight: 50.0, minWidth: 50.0),
                                          child: Icon(
                                            Icons.close,
                                            color: KbackGroundColor,
                                          ),
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                        ),
                                      ),
                                      Text(
                                        "Elija una Imagen Para subir",
                                        style:
                                            kTitleText.copyWith(fontSize: 30),
                                      ),
                                      CircleAvatar(
                                        radius: 200,
                                        backgroundImage: photo == null
                                            ? AssetImage(
                                                "images/defaultprofile.jpg")
                                            : FileImage(photo),
                                        child: Align(
                                          alignment: Alignment.bottomCenter,
                                          child: RawMaterialButton(
                                            fillColor: kPrimaryDarkGrey,
                                            shape: CircleBorder(),
                                            constraints: BoxConstraints(
                                                minHeight: 50.0,
                                                minWidth: 50.0),
                                            child: Icon(
                                              Icons.add,
                                              color: KbackGroundColor,
                                            ),
                                            onPressed: () async {
                                              LoginController.instance
                                                  .chooseImage()
                                                  .then((value) {
                                                setState(() {
                                                  photo = value;
                                                });
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                      Resusable_Button(
                                        text: "Subir",
                                        enableB: photo == null || showProgress
                                            ? false
                                            : true,
                                        textSize: 18,
                                        onPress: () {
                                          if (photo != null) {
                                            setState(() {
                                              showProgress = true;
                                            });
                                            LoginController.instance
                                                .CheckConectivity()
                                                .then((value) {
                                              if (value) {
                                                LoginController.instance
                                                    .tryConnection()
                                                    .then((value) {
                                                  if (value == 200) {
                                                    LoginController.instance
                                                        .uploadPicture(photo)
                                                        .then((value) {
                                                      setState(() {
                                                        PhotUrl =
                                                            LoginController
                                                                .currentUser
                                                                .ProfilepickUrl;
                                                      });
                                                      setState(() {
                                                        showProgress = false;
                                                      });
                                                      PopUpMenu pop =
                                                          PopUpMenu();
                                                      pop.showDialogPopUp(
                                                          "Imagen Actualizada",
                                                          "Se ha actualizado su Imagen de perfil",
                                                          context);
                                                    });
                                                  } else {
                                                    setState(() {
                                                      showProgress = false;
                                                    });
                                                    PopUpMenu pop = PopUpMenu();
                                                    pop.showDialogPopUp(
                                                        "El servidor No responde",
                                                        "En este momento nuestro servidor no responde, trate mas tarde ",
                                                        context);
                                                  }
                                                });
                                              } else {
                                                setState(() {
                                                  showProgress = false;
                                                });
                                                PopUpMenu pop = PopUpMenu();
                                                pop.showDialogPopUp(
                                                    "Sin Conexión a internet",
                                                    "En este momento no posees una conexión a internet ",
                                                    context);
                                              }
                                            });
                                          }
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        },
                      ).then((value) {
                        setState(() {
                          photo;
                        });
                      });
                    },
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
              width: 350.0,
            ),
            Text(
              LoginController.currentUser.name,
              style: kTitleText.copyWith(fontSize: 25),
            ),
            SizedBox(
              height: 20.0,
              width: 350.0,
              child: Divider(
                color: kPrimaryDarkGrey,
              ),
            ),
            Text(
              "Tratamiento",
              style: kTitleText.copyWith(fontSize: 15),
            ),
            Card(
              margin: EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 25.0,
              ),
              elevation: 10.0,
              child: ListTile(
                leading: Icon(
                  Icons.local_hospital_outlined,
                  color: kDarkPink,
                ),
                title: Text(
                  ProgressionController.mh.afliccion,
                  style: TextStyle(
                    color: DarkPurple,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
            Text(
              "Finalizacion Del tratamiento",
              style: kTitleText.copyWith(fontSize: 15),
            ),
            Card(
              margin: EdgeInsets.symmetric(
                vertical: 10.0,
                horizontal: 25.0,
              ),
              elevation: 10.0,
              child: Center(
                child: ListTile(
                  leading: Icon(
                    Icons.calendar_today_outlined,
                    color: kDarkPink,
                  ),
                  title: Text(
                    ProgressionController.mh.Finalizacion.toString(),
                    style: TextStyle(
                      // fontFamily: "SourceSansPro",
                      color: DarkPurple,
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 14.0,
              width: 350.0,
            ),
            Resusable_Button(
              textSize: 15,
              text: "Cerrar Sesión",
              onPress: () {
                final Future<bool> conected =
                    LoginController.instance.CheckConectivity();

                conected.then((value) {
                  if (value) {
                    Future<void> logOut =
                        LoginController.instance.logOutFromApp();
                    logOut.then((value) {
                      Navigator.pushAndRemoveUntil(context,
                          MaterialPageRoute(builder: (BuildContext context) => WelcomeScreen()),
                              (Route<dynamic> route) => route is HomePage
                      );
                     // Navigator.popUntil(context, WelcomeScreen.id);
                    });
                  } else {
                    PopUpMenu pop = PopUpMenu();
                    pop.showDialogPopUp(
                        "Sin Conexion a internet",
                        "En este momento no posees una conexiona internet ",
                        context);
                  }
                });
              },
            )
          ],
        ),
      ),
    );
  }
}

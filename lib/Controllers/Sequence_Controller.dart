import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:minihomelab/Constanse.dart';

class SequenceController {
  final String sessionURL = urlLocal + "sessions/";
  final String exerciseURL = urlLocal + "exercises/Rotine/";

  SequenceController._privateConstructor();

  static final SequenceController _instance =
      SequenceController._privateConstructor();

  static SequenceController get instance => _instance;

  Future rateExercise(String exerciseId, int rate) async {
    final res = await http.post(Uri.parse(exerciseURL + exerciseId),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: json.encode({
          'rate': rate,
        }));
    return res;
  }

  Future sendSession(String userId, Map session) async {
    final res = await http.put(Uri.parse(sessionURL + userId),
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
        body: json.encode({"ejercicio": json.encode(session)}));
    return res;
  }
}

class Statistics {
  ExerciseStatistics exercise;
  UsageStatistics usage;

  Statistics({this.exercise, this.usage});
}

class ExerciseStatistics {
  int numCompleted;
  int numNotCompleted;

  ExerciseStatistics({this.numCompleted, this.numNotCompleted});

  ExerciseStatistics.fromJson(Map<String, dynamic> json) {
    numCompleted = json['numCompleted'] != null ? json['numCompleted'] : 0;
    numNotCompleted =
        json['numNotCompleted'] != null ? json['numNotCompleted'] : 0;
  }
}

class UsageStatistics {
  int lastDaysCount;

  UsageStatistics({this.lastDaysCount});

  UsageStatistics.fromJson(Map<String, dynamic> json) {
    lastDaysCount = json['lastDaysCount'] != null ? json['lastDaysCount'] : 0;
  }
}

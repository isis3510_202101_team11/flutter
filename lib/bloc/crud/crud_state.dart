part of 'crud_bloc.dart';

/// * Estado del bloc
abstract class CrudState {
  const CrudState();

  @override
  List<Object> get props => [];
}

/// * Estado inicial, es ignorado pero necesario
class CrudInitial extends CrudState {}

/// * Estado de cargando, se activa mientras se establece una conexión con el back
/// * Puedo visibilizar cuando estoy cargando un estado desde el back
class CrudLoadInProgress extends CrudState {}

/// * Estado de error: no se especificó el tipo de petición cuando se utilizó el CrudRequestedEvent event
/// * Si el back me retorna que está mal el request
class CrudInvalidRequestType extends CrudState {}

/// * Estado de error: si la petición es PUT or POST, se necesita un body
class CrudInvalidBodyData extends CrudState {}

/// * Estado de error: no se obtuvieron datos del servidor (API inválido generalmente)
class CrudLoadFailure extends CrudState {}

/// * Defino el estado cuando no tengo conexion a internet
class CrudLoadNoConnection extends CrudState {}

/// * Estado Success, se ha logrado establecer una conexión con el back y ha retornado objetos
class CrudLoadSuccess<T> extends CrudState {
  const CrudLoadSuccess({
    @required this.dataObject,
  });

  final T dataObject;

  @override
  List<Object> get props => [dataObject];
}

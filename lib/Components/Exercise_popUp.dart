import 'package:flutter/material.dart';
import 'package:minihomelab/Components/Resusable_Button.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/screens/sequence_screen.dart';

class ExercisePopUp {
  showDialogPopUp(name, time, level, item, context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: name != null ? Center(child: Text(name)) : Text("Name"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text("Tiempo del Ejercicio :"),
              Text(time),
              Container(
                width: 100,
                alignment: Alignment.center,
                child: Resusable_Button(
                  text: "OK",
                  textSize: 15,
                  onPress: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Center(
                        child: Text(
                          "Cancelar",
                          style: kMediumTextStyle,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: DarkPurple,
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () async{
                        ProgressionController.currentExercise =  ProgressionController.exer[item];
                        await DataBaseProvider.instance.updateExer(ProgressionController.exer[item]);
                        Navigator.of(context).pop();
                        Navigator.pushNamed(
                          context,
                          SequenceScreen.id,
                          arguments: SequenceArguments(
                            level: level,
                            number: item+1,
                            name: name,
                            seconds: ProgressionController.exer[item].time,
                          ),
                        );
                      },
                      child: Center(
                        child: Text(
                          "Iniciar",
                          style: kMediumTextStyle,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: DarkPurple,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          elevation: 10,
          backgroundColor: KbackGroundColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        );
      },
    );
  }
}

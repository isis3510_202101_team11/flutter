//Declaro la librería
library json_parser;
//Clase abstracta que va a servir para parsear desde Json a un objeto desde el back
export 'Object_decoder.dart';
abstract class JsonParser <T>{

const JsonParser();

//Método asincrónico que recibe la rta json desde el back y lo transforma en un future
Future <T> parseFromJson(String json);
}
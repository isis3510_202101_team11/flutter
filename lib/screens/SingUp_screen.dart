import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minihomelab/Components/Reusable_Card.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Components/Resusable_Button.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'Home_Page.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:minihomelab/Components/PopUpMenu.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';

class SignUpScreen extends StatefulWidget {
  static const String id = 'signup_screen';

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String errorMesage = "";
  String email;
  String password;
  String passwordConfirm;
  String Name;
  DateTime birttDay = DateTime.now();
  MainAxisAlignment alingment = MainAxisAlignment.spaceAround;
  bool keyboard = false;
  bool showSpinner = false;
  int state = 5;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          keyboard = visible;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: showSpinner,
      child: Scaffold(
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Hero(
                tag: "iconh",
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Icon(
                    Icons.medical_services_outlined,
                    color: Color(0xFF3E4259),
                    size: 40.0,
                  ),
                ),
              ),
              Text(
                "Mini-Home Lab",
                style: kTitleText.copyWith(fontSize: 20.0),
              ),
              Center(
                child: ReusableCard(
                  backColor: Color(0xFF3E4259),
                  height: keyboard == false ? 500.0 : 285,
                  children: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Center(
                        child: Text(
                          errorMesage,
                          style: kTitleText.copyWith(
                              color: Colors.red.shade500, fontSize: 20),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0),
                        child: TextField(
                          keyboardType: TextInputType.emailAddress,
                          textAlign: TextAlign.center,
                          decoration: kTextFieldDecoration.copyWith(
                              hintText: "Correo Electronico"),
                          onChanged: (value) {
                            email = value;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0),
                        child: TextField(
                          obscureText: true,
                          textAlign: TextAlign.center,
                          decoration:
                              kTextFieldDecoration.copyWith(hintText: "Clave"),
                          onChanged: (value) {
                            password = value;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0),
                        child: TextField(
                          obscureText: true,
                          textAlign: TextAlign.center,
                          decoration: kTextFieldDecoration.copyWith(
                              hintText: "Confirmar Clave"),
                          onChanged: (value) {
                            passwordConfirm = value;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0),
                        child: TextField(
                          textAlign: TextAlign.center,
                          decoration: kTextFieldDecoration.copyWith(
                              hintText: "Ingrese Su nombre"),
                          onChanged: (value) {
                            Name = value;
                          },
                        ),
                      ),
                      Visibility(
                        visible: !keyboard,
                        child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: ListTile(
                              title: Text(
                                "Fecha de Nacimiento  ${birttDay.year}/${birttDay.month}/${birttDay.day}",
                                style: kMediumTextStyle,
                                textAlign: TextAlign.center,
                              ),
                              trailing: Icon(
                                Icons.keyboard_arrow_down_outlined,
                                color: Colors.white,
                              ),
                              onTap: () {
                                pickDate();
                              },
                            )),
                      ),
                      Hero(
                        tag: "LoginB",
                        child: Visibility(
                          visible: !keyboard,
                          child: Resusable_Button(
                              text: "Registrarse",
                              textSize: keyboard == true ? 14 : 18,
                              onPress: () async {
                                setState(() {
                                  showSpinner = true;
                                });

                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          "¿Has probado algún tratamiento alternativo?",
                                          style:
                                              kTitleText.copyWith(fontSize: 25),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        content: Container(
                                          height: 200,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            children: [
                                              Expanded(
                                                child: CupertinoPicker(
                                                  itemExtent: 30,
                                                  useMagnifier: true,
                                                  backgroundColor:
                                                      KbackGroundColor,
                                                  onSelectedItemChanged:
                                                      (inte) {
                                                    state = 10 - inte;
                                                  },
                                                  children: [
                                                    Text("Niguna"),
                                                    Text("Acupuntura"),
                                                    Text("Aterterapia"),
                                                    Text("Hidroterapia"),
                                                    Text("Hipoterapia"),
                                                    Text("Musicoterapia"),
                                                    Text("Psicodanza"),
                                                    Text("Risoterapia"),
                                                    Text("Ultrasonido"),
                                                    Text("Homeopatía"),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        actions: [
                                          Container(
                                            height: 60,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width +
                                                20,
                                            alignment: Alignment.center,
                                            child: Resusable_Button(
                                              textSize: 15,
                                              text: "Ok",
                                              onPress: () {
                                                Navigator.pop(context);
                                              },
                                            ),
                                          ),
                                        ],
                                      );
                                    }).then((value) {
                                  LoginController.instance
                                      .CheckConectivity()
                                      .then((value) {
                                    if (value) {
                                      LoginController.instance
                                          .tryConnection()
                                          .then((value) {
                                        if (value == 200) {
                                          LoginController.instance
                                              .registerUser(email, password,
                                                  passwordConfirm)
                                              .then((value) {
                                            if (value[0] == true) {
                                              LoginController.instance
                                                  .addUserData(Name, birttDay)
                                                  .then((value) {
                                                if (value != null) {
                                                  LoginController.instance
                                                      .getUser()
                                                      .then((value) {
                                                    if (value != null) {
                                                      ProgressionController
                                                          .instance
                                                          .RegisterMh(
                                                              LoginController
                                                                  .currentUser
                                                                  .userId,
                                                              state)
                                                          .then((value) {
                                                        ProgressionController
                                                            .instance
                                                            .getUserMH(
                                                                LoginController
                                                                    .currentUser
                                                                    .userId)
                                                            .then((value) {
                                                          if (value != null) {
                                                            Navigator.pushNamed(
                                                                context,
                                                                HomePage.id);
                                                          } else {
                                                            setState(() {
                                                              errorMesage =
                                                                  "A ocurrido un error Con Nuestros servidores ";
                                                              showSpinner =
                                                                  false;
                                                            });
                                                          }
                                                        });
                                                      });
                                                    }
                                                  });
                                                }
                                              });
                                            } else {
                                              setState(() {
                                                errorMesage = value[1];
                                                showSpinner = false;
                                              });
                                            }
                                          });
                                        } else {
                                          setState(() {
                                            showSpinner = false;
                                          });
                                          PopUpMenu pop = PopUpMenu();
                                          pop.showDialogPopUp(
                                              "El servidor No responde",
                                              "En este momento nuestro servidor no responde, trate mas tarde ",
                                              context);
                                        }
                                      });
                                    } else {
                                      setState(() {
                                        showSpinner = false;
                                      });
                                      PopUpMenu pop = PopUpMenu();
                                      pop.showDialogPopUp(
                                          "Sin Conexión a internet",
                                          "En este momento no posees una conexión a internet ",
                                          context);
                                    }
                                  });
                                });
                              }),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  pickDate() async {
    DateTime date = await showDatePicker(
        context: context,
        initialDate: birttDay,
        firstDate: DateTime(DateTime.now().year - 100),
        lastDate: DateTime(DateTime.now().year + 5));

    if (date != null) {
      setState(() {
        birttDay = date;
      });
    }
  }
}

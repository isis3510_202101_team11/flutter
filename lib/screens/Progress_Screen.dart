import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../Components/Reusable_Card.dart';
import '../Constanse.dart';
import '../bloc/crud/crud_bloc.dart';
import '../models/Progress_Screen/DataProgressScreenModel.dart';
import '../models/Progress_Screen/ProgressScreenModelParser.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import '../Controllers/Login_Controller.dart';

class ProgressScreen extends StatefulWidget {
  static const String id = 'progress_screen';

  @override
  _ProgressScreenState createState() => _ProgressScreenState();
}

class _ProgressScreenState extends State<ProgressScreen> {
  /// Index selección de tab entre ROM, Fuerza y T reacción
  int index = 0;

  /// Data ROM
  List<Map<String, double>> dataRomWeek = [];
  List<Map<String, double>> dataRomMonth = [];
  List<Map<String, String>> destacdosROM = [
    <String, String>{'title': 'Aumento en ROM total', 'value': '35%'},
    <String, String>{
      'title': 'Aumento en ROM respecto a la última semana',
      'value': '10%'
    },
  ];
  List<Map<String, String>> destacdosROMMonth = [
    <String, String>{'title': 'Aumento en ROM total', 'value': '35%'},
    <String, String>{
      'title': 'Aumento en ROM respecto al último mes',
      'value': '12%'
    },
  ];

  /// Data FUERZA
  List<Map<String, double>> dataFuerzaWeek = [];
  List<Map<String, double>> dataFuerzaMonth = [];
  List<Map<String, String>> destacadosFuerza = [
    <String, String>{'title': 'Aumento en fuerza total', 'value': '80KPA'},
    <String, String>{
      'title': 'Aumento en fuerza respecto a la última semana',
      'value': '38KPA'
    },
  ];
  List<Map<String, String>> destacadosFuerzaMonth = [
    <String, String>{'title': 'Aumento en fuerza total', 'value': '80KPA'},
    <String, String>{
      'title': 'Aumento en fuerza respecto al último mes',
      'value': '55KPA'
    },
  ];

  /// Data TIEMPO REACCION
  List<Map<String, double>> dataTiempoRWeek = [];
  List<Map<String, double>> dataTiempoRMonth = [];
  List<Map<String, String>> destacadosTiempo = [
    <String, String>{'title': 'Tiempo promedio', 'value': '6\'21\''},
    <String, String>{'title': 'Tiempo record', 'value': '4\'15\''},
  ];
  List<Map<String, String>> destacadosTiempoMonth = [
    <String, String>{'title': 'Tiempo promedio', 'value': '7\'30\''},
    <String, String>{'title': 'Tiempo record', 'value': '4\'15\''},
  ];

  /// Selección de tab
  void _onSelectTab(int value) {
    setState(() {
      index = value;
    });
  }

  ///Antes de renderizar la pantalla se ejecuta
  @override
  void initState() {
    super.initState();

    String userId = LoginController.userId;
    BlocProvider.of<CrudBloc<DataProgressScreenModel>>(context).add(
      CrudRequestedEvent<DataProgressScreenModel>(
        api: 'progress/$userId',
        dataParser: const ProgressScreenModelParser(),
        requestType: RequestType.getRequest,
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Progreso'),
        centerTitle: true,
        brightness: Brightness.dark,
      ),
      body: BlocBuilder<CrudBloc<DataProgressScreenModel>, CrudState>(
        builder: (context, state) {
          if (state is CrudLoadNoConnection) {
            return Center(
              child: Text(
                'No tiene conexión a internet',
              )
            );
          }
          else if (state is CrudLoadInProgress) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is CrudLoadFailure) {
            return Center(
              child: Text(
                'Error en conexión con el servidor :(',
              ),
            );
          } else if (state is CrudLoadSuccess<DataProgressScreenModel>) {

            FirebaseAnalytics().logEvent(name: 'progress_event',parameters:null);
            final data = state.dataObject;

            dataRomWeek = data?.dataRomWeek ?? [];
            dataRomMonth = data?.dataRomMonth ?? [];
            destacdosROM = data?.destacadosRomWeek ?? [];
            destacdosROMMonth = data?.destacadosRomMonth ?? [];

            dataTiempoRWeek = data?.dataTiempoRWeek ?? [];
            dataTiempoRMonth = data?.dataTiempoRMonth ?? [];
            destacadosTiempo = data?.destacadosTiempoRWeek ?? [];
            destacadosTiempoMonth = data?.destacadosTiempoRMonth ?? [];

            dataFuerzaMonth = data?.dataFuerzaMonth ?? [];
            dataFuerzaWeek = data?.dataFuerzaWeek ?? [];
            destacadosFuerza = data?.destacadosFuerzaWeek ?? [];
            destacadosFuerzaMonth = data?.destacadosFuerzaMonth ?? [];

            return Column(
              children: [
                Expanded(
                  child: CustomScrollView(
                    slivers: [
                      /// * Barra Superior Selección ROM, FUERZA. T DE REACCIÓN
                      _BarraSeleccionWidget(
                        index: index,
                        onSelectTab: _onSelectTab,
                      ),

                      /// * Contenido ROM
                      if (index == 0)
                        _ContenidoRomWidget(
                          dataWeek: dataRomWeek,
                          dataMonth: dataRomMonth,
                          destacados: destacdosROM,
                          destacadosMonth: destacdosROMMonth,
                        ),

                      /// * Contenido FUERZA
                      if (index == 1)
                        _ContenidoFuerzaWidget(
                          dataWeek: dataFuerzaWeek,
                          dataMonth: dataFuerzaMonth,
                          destacados: destacadosFuerza,
                          destacadosMonth: destacadosFuerzaMonth,
                        ),

                      /// * Contenido T REACCION
                      if (index == 2)
                        _ContenidoTiempoReaccionWidget(
                          dataWeek: dataTiempoRWeek,
                          dataMonth: dataTiempoRMonth,
                          destacados: destacadosTiempo,
                          destacadosMonth: destacadosTiempoMonth,
                        ),

                      SliverToBoxAdapter(
                        child: SizedBox(
                          height: 32,
                        ),
                      )
                    ],
                  ),
                ),

                /// * Bottom nav icons
                SafeArea(child: _BottomNavBar()),
              ],
            );
          } else {
            return Column(
              children: [
                Text(
                  'Vuelve a intentar',
                ),
              ],
            );
          }
        },
      ),
    );
  }
}

/// * Barra Superior Selección ROM, FUERZA. T DE REACCIÓN
class _BarraSeleccionWidget extends StatelessWidget {
  const _BarraSeleccionWidget({
    @required this.index,
    @required this.onSelectTab,
  });

  /// Tab seleccionada
  final int index;

  /// Seleccion de tab
  final void Function(int) onSelectTab;

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 32,
      ),
      sliver: SliverToBoxAdapter(
        child: Container(
          width: double.infinity,
          height: 42,
          decoration: BoxDecoration(
            color: KbackGroundColor,
            borderRadius: BorderRadius.circular(32),
            border: Border.all(
              width: 2,
              color: Color(0xFF675F73),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              /// ROM
              _TabSeleccionWidget(
                onSelectTab: onSelectTab,
                index: index,
                title: 'ROM',
                id: 0,
              ),

              /// FUERZA
              _TabSeleccionWidget(
                onSelectTab: onSelectTab,
                index: index,
                title: 'Fuerza',
                id: 1,
              ),

              /// T DE REACCION
              _TabSeleccionWidget(
                onSelectTab: onSelectTab,
                index: index,
                title: 'T. de reacción',
                id: 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/// * Tab de selección
class _TabSeleccionWidget extends StatelessWidget {
  const _TabSeleccionWidget({
    @required this.onSelectTab,
    @required this.index,
    @required this.title,
    @required this.id,
  });

  /// Function seleccion de tab
  final void Function(int p1) onSelectTab;

  /// Index seleccionado
  final int index;

  /// Titulo
  final String title;

  /// ID
  final int id;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: GestureDetector(
        onTap: () {
          onSelectTab(id);
        },
        child: Container(
          decoration: index != id
              ? null
              : BoxDecoration(
                  color: Color(0xFF675F73),
                  borderRadius: BorderRadius.circular(32),
                ),
          child: Center(
            child: Text(
              title,
              style: index != id
                  ? null
                  : TextStyle(
                      color: KbackGroundColor,
                      fontWeight: FontWeight.bold,
                    ),
            ),
          ),
        ),
      ),
    );
  }
}

/// * Bottom navigation bar
class _BottomNavBar extends StatelessWidget {
  const _BottomNavBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ReusableCard(
            marginAll: 5.0,
            backColor: Color(0xFF675F73),
            height: 65.0,
            children: Icon(
              Icons.bar_chart_outlined,
              color: KbackGroundColor,
              size: 50.0,
            ),
          ),
        ),
        Expanded(
          child: ReusableCard(
            marginAll: 5.0,
            backColor: Color(0xFF675F73),
            height: 65.0,
            children: Icon(
              Icons.date_range_outlined,
              color: KbackGroundColor,
              size: 50.0,
            ),
          ),
        ),
        Expanded(
          child: ReusableCard(
            marginAll: 5.0,
            backColor: Color(0xFF675F73),
            height: 65.0,
            children: Icon(
              Icons.home_outlined,
              color: KbackGroundColor,
              size: 50.0,
            ),
          ),
        ),
        Expanded(
          child: ReusableCard(
            marginAll: 5.0,
            backColor: Color(0xFF675F73),
            height: 65.0,
            children: Icon(
              Icons.fitness_center_outlined,
              color: KbackGroundColor,
              size: 50.0,
            ),
          ),
        ),
        Expanded(
          child: ReusableCard(
            marginAll: 5.0,
            backColor: Color(0xFF675F73),
            height: 65.0,
            children: Icon(
              Icons.call_outlined,
              color: KbackGroundColor,
              size: 50.0,
            ),
          ),
        ),
      ],
    );
  }
}

/// * Contenido ROM
class _ContenidoRomWidget extends StatefulWidget {
  _ContenidoRomWidget({
    @required this.dataWeek,
    @required this.dataMonth,
    @required this.destacados,
    @required this.destacadosMonth,
  });

  /// Data grafica
  final List<Map<String, double>> dataWeek;
  final List<Map<String, double>> dataMonth;

  /// Data destacados
  final List<Map<String, String>> destacados;
  final List<Map<String, String>> destacadosMonth;

  @override
  __ContenidoRomWidgetState createState() => __ContenidoRomWidgetState();
}

class __ContenidoRomWidgetState extends State<_ContenidoRomWidget> {
  /// Index selección de tab entre ROM, Fuerza y T reacción
  int index = 1;

  /// Selección de tab
  void _onSelectTab(int value) {
    setState(() {
      index = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      sliver: SliverToBoxAdapter(
        child: ShrinkWrappingViewport(
          offset: ViewportOffset.zero(),
          slivers: [
            /// * GRÁFICA
            SliverToBoxAdapter(
              child: index == 0
                  ? Text('Mar 1 - Mar 30, 2021')
                  : Text('Mar 1 - 7, 2021'),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 0, right: 12),
                height: 200,
                child: BarChart(
                  BarChartData(
                    maxY: 120,
                    minY: 0,
                    titlesData: FlTitlesData(
                        show: true,
                        rightTitles: SideTitles(
                          getTextStyles: (value) => const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                            color: Color(0xFF675F73),
                          ),
                          showTitles: true,
                          margin: 18,
                          reservedSize: 18,
                          getTitles: (value) {
                            if (value == 90) {
                              return '90°';
                            } else if (value == 120) {
                              return '120°';
                            } else {
                              return '';
                            }
                          },
                        ),
                        leftTitles: SideTitles(
                          showTitles: false,
                        ),
                        bottomTitles: SideTitles(
                          showTitles: true,
                          getTitles: (value) {
                            if (index == 0) {
                              return value % 5 == 0 ? '${value.toInt()}' : '';
                            }
                            return '${value.toInt()}';
                          },
                        )),
                    barGroups: index == 0
                        ? widget.dataMonth
                            .map(
                              (e) => BarChartGroupData(
                                x: e['x']?.toInt() ?? 0,
                                barRods: [
                                  BarChartRodData(
                                    y: e['y'] ?? 0,
                                    colors: [
                                      Color(0xFF675F73),
                                    ],
                                    width: 6,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(3),
                                      topRight: Radius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList()
                        : widget.dataWeek
                            .map(
                              (e) => BarChartGroupData(
                                x: e['x']?.toInt() ?? 0,
                                barRods: [
                                  BarChartRodData(
                                    y: e['y'] ?? 0,
                                    colors: [
                                      Color(0xFF675F73),
                                    ],
                                    width: 32,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(3),
                                      topRight: Radius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList(),
                  ),
                  swapAnimationDuration: Duration(
                    milliseconds: 150,
                  ), // Optional
                  swapAnimationCurve: Curves.linear, // Optional
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 64,
                ),
                width: double.infinity,
                height: 42,
                decoration: BoxDecoration(
                  color: KbackGroundColor,
                  borderRadius: BorderRadius.circular(32),
                  border: Border.all(
                    width: 2,
                    color: Color(0xFF675F73),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    /// ROM
                    _TabSeleccionWidget(
                      onSelectTab: _onSelectTab,
                      index: index,
                      title: 'Mes',
                      id: 0,
                    ),

                    /// FUERZA
                    _TabSeleccionWidget(
                      onSelectTab: _onSelectTab,
                      index: index,
                      title: 'Semana',
                      id: 1,
                    ),
                  ],
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 24,
              ),
            ),

            /// * DESTACADOS
            SliverToBoxAdapter(
              child: Text(
                'Destacado',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            if (index == 1)
              ...widget.destacados
                  .map(
                    (e) => SliverPadding(
                      padding: const EdgeInsets.only(bottom: 8),
                      sliver: SliverToBoxAdapter(
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          marginAll: 0,
                          height: 112,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                e['title'],
                                style: TextStyle(
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                e['value'],
                                style: kBigTextStyle.copyWith(
                                  fontSize: 64,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),

            if (index == 0)
              ...widget.destacadosMonth
                  .map(
                    (e) => SliverPadding(
                      padding: const EdgeInsets.only(bottom: 8),
                      sliver: SliverToBoxAdapter(
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          marginAll: 0,
                          height: 112,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                e['title'],
                                style: TextStyle(
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                e['value'],
                                style: kBigTextStyle.copyWith(
                                  fontSize: 64,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
          ],
        ),
      ),
    );
  }
}

/// * Contenido FUERZA
class _ContenidoFuerzaWidget extends StatefulWidget {
  _ContenidoFuerzaWidget({
    @required this.dataWeek,
    @required this.dataMonth,
    @required this.destacados,
    @required this.destacadosMonth,
  });

  /// Data grafica
  final List<Map<String, double>> dataWeek;
  final List<Map<String, double>> dataMonth;

  /// Data destacados
  final List<Map<String, String>> destacados;
  final List<Map<String, String>> destacadosMonth;

  @override
  __ContenidoFuerzaWidgetState createState() => __ContenidoFuerzaWidgetState();
}

class __ContenidoFuerzaWidgetState extends State<_ContenidoFuerzaWidget> {
  /// Index selección de tab entre ROM, Fuerza y T reacción
  int index = 1;

  /// Selección de tab
  void _onSelectTab(int value) {
    setState(() {
      index = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      sliver: SliverToBoxAdapter(
        child: ShrinkWrappingViewport(
          offset: ViewportOffset.zero(),
          slivers: [
            /// * GRÁFICA
            SliverToBoxAdapter(
              child: index == 0
                  ? Text('Mar 1 - Mar 30, 2021')
                  : Text('Mar 1 - 7, 2021'),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 0, right: 12),
                height: 200,
                child: BarChart(
                  BarChartData(
                    maxY: 300,
                    minY: 0,
                    titlesData: FlTitlesData(
                      show: true,
                      rightTitles: SideTitles(
                        getTextStyles: (value) => const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: Color(0xFF675F73),
                        ),
                        showTitles: true,
                        margin: 18,
                        reservedSize: 14,
                        getTitles: (value) {
                          if (value == 150) {
                            return '150kPa';
                          } else if (value == 300) {
                            return '300kPa';
                          } else {
                            return '';
                          }
                        },
                      ),
                      leftTitles: SideTitles(
                        showTitles: false,
                      ),
                      bottomTitles: SideTitles(
                        showTitles: true,
                        getTitles: (value) {
                          if (index == 0) {
                            return value % 5 == 0 ? '${value.toInt()}' : '';
                          }
                          return '${value.toInt()}';
                        },
                      ),
                    ),
                    barGroups: index == 0
                        ? widget.dataMonth
                            .map(
                              (e) => BarChartGroupData(
                                x: e['x']?.toInt() ?? 0,
                                barRods: [
                                  BarChartRodData(
                                    y: e['y'] ?? 0,
                                    colors: [
                                      Color(0xFF675F73),
                                    ],
                                    width: 6,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(3),
                                      topRight: Radius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList()
                        : widget.dataWeek
                            .map(
                              (e) => BarChartGroupData(
                                x: e['x']?.toInt() ?? 0,
                                barRods: [
                                  BarChartRodData(
                                    y: e['y'] ?? 0,
                                    colors: [
                                      Color(0xFF675F73),
                                    ],
                                    width: 32,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(3),
                                      topRight: Radius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList(),
                  ),
                  swapAnimationDuration: Duration(
                    milliseconds: 150,
                  ), // Optional
                  swapAnimationCurve: Curves.linear, // Optional
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 64,
                ),
                width: double.infinity,
                height: 42,
                decoration: BoxDecoration(
                  color: KbackGroundColor,
                  borderRadius: BorderRadius.circular(32),
                  border: Border.all(
                    width: 2,
                    color: Color(0xFF675F73),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    /// ROM
                    _TabSeleccionWidget(
                      onSelectTab: _onSelectTab,
                      index: index,
                      title: 'Mes',
                      id: 0,
                    ),

                    /// FUERZA
                    _TabSeleccionWidget(
                      onSelectTab: _onSelectTab,
                      index: index,
                      title: 'Semana',
                      id: 1,
                    ),
                  ],
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 24,
              ),
            ),

            /// * DESTACADOS
            SliverToBoxAdapter(
              child: Text(
                'Destacado',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            if (index == 1)
              ...widget.destacados
                  .map(
                    (e) => SliverPadding(
                      padding: const EdgeInsets.only(bottom: 8),
                      sliver: SliverToBoxAdapter(
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          marginAll: 0,
                          height: 112,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                e['title'],
                                style: TextStyle(
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                e['value'],
                                style: kBigTextStyle.copyWith(
                                  fontSize: 64,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),

            if (index == 0)
              ...widget.destacadosMonth
                  .map(
                    (e) => SliverPadding(
                      padding: const EdgeInsets.only(bottom: 8),
                      sliver: SliverToBoxAdapter(
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          marginAll: 0,
                          height: 112,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                e['title'],
                                style: TextStyle(
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                e['value'],
                                style: kBigTextStyle.copyWith(
                                  fontSize: 64,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
          ],
        ),
      ),
    );
  }
}

/// * Contenido T REACCION
class _ContenidoTiempoReaccionWidget extends StatefulWidget {
  _ContenidoTiempoReaccionWidget({
    @required this.dataWeek,
    @required this.dataMonth,
    @required this.destacados,
    @required this.destacadosMonth,
  });

  /// Data grafica
  final List<Map<String, double>> dataWeek;
  final List<Map<String, double>> dataMonth;

  /// Data destacados
  final List<Map<String, String>> destacados;
  final List<Map<String, String>> destacadosMonth;

  @override
  __ContenidoTiempoReaccionidgetState createState() =>
      __ContenidoTiempoReaccionidgetState();
}

class __ContenidoTiempoReaccionidgetState
    extends State<_ContenidoTiempoReaccionWidget> {
  /// Index selección de tab entre ROM, Fuerza y T reacción
  int index = 1;

  /// Selección de tab
  void _onSelectTab(int value) {
    setState(() {
      index = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      sliver: SliverToBoxAdapter(
        child: ShrinkWrappingViewport(
          offset: ViewportOffset.zero(),
          slivers: [
            /// * GRÁFICA
            SliverToBoxAdapter(
              child: index == 0
                  ? Text('Mar 1 - Mar 30, 2021')
                  : Text('Mar 1 - 7, 2021'),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                padding: const EdgeInsets.only(left: 0, right: 12),
                height: 200,
                child: BarChart(
                  BarChartData(
                    maxY: 10,
                    minY: 0,
                    titlesData: FlTitlesData(
                      show: true,
                      rightTitles: SideTitles(
                        getTextStyles: (value) => const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15,
                          color: Color(0xFF675F73),
                        ),
                        showTitles: true,
                        margin: 18,
                        reservedSize: 18,
                        getTitles: (value) {
                          if (value == 10) {
                            return '10\'';
                          } else if (value == 5) {
                            return '5\'';
                          } else {
                            return '';
                          }
                        },
                      ),
                      leftTitles: SideTitles(
                        showTitles: false,
                      ),
                      bottomTitles: SideTitles(
                        showTitles: true,
                        getTitles: (value) {
                          if (index == 0) {
                            return value % 5 == 0 ? '${value.toInt()}' : '';
                          }
                          return '${value.toInt()}';
                        },
                      ),
                    ),
                    barGroups: index == 0
                        ? widget.dataMonth
                            .map(
                              (e) => BarChartGroupData(
                                x: e['x']?.toInt() ?? 0,
                                barRods: [
                                  BarChartRodData(
                                    y: e['y'] ?? 0,
                                    colors: [
                                      Color(0xFF675F73),
                                    ],
                                    width: 6,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(3),
                                      topRight: Radius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList()
                        : widget.dataWeek
                            .map(
                              (e) => BarChartGroupData(
                                x: e['x']?.toInt() ?? 0,
                                barRods: [
                                  BarChartRodData(
                                    y: e['y'] ?? 0,
                                    colors: [
                                      Color(0xFF675F73),
                                    ],
                                    width: 32,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(3),
                                      topRight: Radius.circular(3),
                                    ),
                                  ),
                                ],
                              ),
                            )
                            .toList(),
                  ),
                  swapAnimationDuration: Duration(
                    milliseconds: 150,
                  ), // Optional
                  swapAnimationCurve: Curves.linear, // Optional
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverToBoxAdapter(
              child: Container(
                margin: const EdgeInsets.symmetric(
                  horizontal: 64,
                ),
                width: double.infinity,
                height: 42,
                decoration: BoxDecoration(
                  color: KbackGroundColor,
                  borderRadius: BorderRadius.circular(32),
                  border: Border.all(
                    width: 2,
                    color: Color(0xFF675F73),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    /// ROM
                    _TabSeleccionWidget(
                      onSelectTab: _onSelectTab,
                      index: index,
                      title: 'Mes',
                      id: 0,
                    ),

                    /// FUERZA
                    _TabSeleccionWidget(
                      onSelectTab: _onSelectTab,
                      index: index,
                      title: 'Semana',
                      id: 1,
                    ),
                  ],
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 24,
              ),
            ),

            /// * DESTACADOS
            SliverToBoxAdapter(
              child: Text(
                'Destacado',
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            if (index == 1)
              ...widget.destacados
                  .map(
                    (e) => SliverPadding(
                      padding: const EdgeInsets.only(bottom: 8),
                      sliver: SliverToBoxAdapter(
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          marginAll: 0,
                          height: 112,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                e['title'],
                                style: TextStyle(
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                e['value'],
                                style: kBigTextStyle.copyWith(
                                  fontSize: 64,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),

            if (index == 0)
              ...widget.destacadosMonth
                  .map(
                    (e) => SliverPadding(
                      padding: const EdgeInsets.only(bottom: 8),
                      sliver: SliverToBoxAdapter(
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          marginAll: 0,
                          height: 112,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                e['title'],
                                style: TextStyle(
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                e['value'],
                                style: kBigTextStyle.copyWith(
                                  fontSize: 64,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
          ],
        ),
      ),
    );
  }
}

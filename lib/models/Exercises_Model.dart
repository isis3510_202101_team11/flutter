import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';

class exercises {

  int id;
  String userId;
  String ExerId;
  String name;
  int NumRepeticions;
  int NumSeries;
  int ResX;
  int ResY;
  int ResZ;
  int time;
  bool isDone;
  DateTime Create;
  String URL;
  String Explicacion;

  exercises({
    this.id,
    this.userId,
    this.ExerId,
    this.name,
    this.NumRepeticions,
    this.NumSeries,
    this.ResX,
    this.ResY,
    this.ResZ,
    this.time,
    this.isDone = false,
    @required this.Create,
    this.URL ="",
    this.Explicacion = ""
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DataBaseProvider.COLUMN_UserID: userId,
      DataBaseProvider.COLUMN_NAME: name,
      DataBaseProvider.COLUMN_ExId:ExerId,
      DataBaseProvider.TABLE_EX_NUMREP: NumRepeticions,
      DataBaseProvider.TABLE_EX_NUMSER: NumSeries,
      DataBaseProvider.TABLE_EX_X: ResX,
      DataBaseProvider.TABLE_EX_Y: ResY,
      DataBaseProvider.TABLE_EX_Z: ResZ,
      DataBaseProvider.TABLE_EX_TIME: time,
      DataBaseProvider.TABLE_EX_ISDONE: isDone==true?1:0,
      DataBaseProvider.CREATE: DateTime.now().millisecondsSinceEpoch,
      DataBaseProvider.IMAGEURL:URL,
      DataBaseProvider.EXPLICACION:Explicacion,
    };
    if (id != null) {
      map[DataBaseProvider.COLUMN_ID] = id;
    }

    return map;
  }

  exercises.fromMap(Map<String, dynamic> map) {
    id = map[DataBaseProvider.COLUMN_ID];
    userId = map[DataBaseProvider.COLUMN_UserID];
    name = map[DataBaseProvider.COLUMN_NAME];
    ExerId = map[DataBaseProvider.COLUMN_ExId];
    NumRepeticions = map[DataBaseProvider.TABLE_EX_NUMREP];
    NumSeries = map[DataBaseProvider.TABLE_EX_NUMSER];
    ResX = map[DataBaseProvider.TABLE_EX_X];
    ResY = map[DataBaseProvider.TABLE_EX_Y];
    ResZ = map[DataBaseProvider.TABLE_EX_Z];
    time = map[DataBaseProvider.TABLE_EX_TIME];
    isDone = map[DataBaseProvider.TABLE_EX_ISDONE]==1?true:false;
    Create = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.CREATE]);
    URL = map[DataBaseProvider.IMAGEURL];
    Explicacion = map[DataBaseProvider.EXPLICACION];

  }

  String ToString()
  {
    return userId.toString() + " "+name.toString() + " "+NumRepeticions.toString() + " " + Create.toString();
  }
}
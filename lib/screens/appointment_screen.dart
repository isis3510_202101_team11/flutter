import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../Components/Reusable_Card.dart';
import '../Constanse.dart';
import '../Controllers/Login_Controller.dart';
import '../bloc/crud/crud_bloc.dart';
import '../models/Appointment/data_appointment_model.dart';
import '../models/Appointment/data_appointment_parser.dart';



class AppointmentScreen extends StatefulWidget {
  const AppointmentScreen({Key key}) : super(key: key);

  static const String id = 'appointment_screen';

  @override
  _AppointmentScreenState createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  String userId = LoginController.userId;
  int page = 0;
  String idAp;
  void togglePage() {
    setState(() {
      page = page == 0 ? 1 : 0;
    });
  }

  void cancelAppointment(String idApp) {
    FirebaseAnalytics().logEvent(name: 'cancel_event',parameters:null);
    BlocProvider.of<CrudBloc<DataAppointmentModel>>(context).add(
      CrudRequestedEvent<DataAppointmentModel>(
          api: 'appointment/update/$idApp',
          dataParser: const DataAppointmentParser(),
          requestType: RequestType.putRequest,
          body: <String, int>{'Estate': 0}),
    );
  }

  void reschedule(String date, String idApp) {
    FirebaseAnalytics().logEvent(name: 'reschedule_event',parameters:null);
    BlocProvider.of<CrudBloc<DataAppointmentModel>>(context).add(
      CrudRequestedEvent<DataAppointmentModel>(
          api: 'appointment/update/$idApp',
          dataParser: const DataAppointmentParser(),
          requestType: RequestType.putRequest,
          body: <String, dynamic>{'Estate': 2, 'ApointDate': date}),
    );
  }

  @override
  void initState() {
    super.initState();
    
    BlocProvider.of<CrudBloc<DataAppointmentModel>>(context).add(
      CrudRequestedEvent<DataAppointmentModel>(
        api: 'appointment/$userId',
        dataParser: const DataAppointmentParser(),
        requestType: RequestType.getRequest,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Citas'),
        centerTitle: true,
        brightness: Brightness.dark,
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: page == 0
                ? InkWell(
                    onTap: togglePage,
                    child: const Icon(
                      Icons.add,
                      size: 32,
                    ),
                  )
                : InkWell(
                    onTap: togglePage,
                    child: const Center(
                        child: Text(
                      'Cancelar',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 18),
                    )),
                  ),
          )
        ],
      ),
      body: page == 0
          ? _AppointmentBuilder(
              reschedule: reschedule,
              cancelAppointment: cancelAppointment,
              idAp: idAp,
            )
          : _AppointmentRegister(
              togglePage: togglePage,
              userId: userId,
            ),
    );
  }
}

// * Builder
class _AppointmentBuilder extends StatefulWidget {
  const _AppointmentBuilder({
    @required this.reschedule,
    @required this.cancelAppointment,
    @required this.idAp,
  });

  final Function(String date, String idAp) reschedule;
  final Function(String idApp) cancelAppointment;
  final String idAp;
  @override
  __AppointmentBuilderState createState() => __AppointmentBuilderState();
}

class __AppointmentBuilderState extends State<_AppointmentBuilder> {
  DateTime selectedDate = DateTime.now();
  String idAp;
  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
      widget.reschedule(selectedDate.toString(),idAp);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CrudBloc<DataAppointmentModel>, CrudState>(
      builder: (context, state) {
        if (state is CrudLoadInProgress) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is CrudLoadFailure) {
          return Center(
            child: Text(
              'Error en conexión con el servidor :(',
              style: kMediumTextStyle.copyWith(
                color: kPrimaryDarkGrey,
              ),
            ),
          );
        } else if (state is CrudLoadSuccess<DataAppointmentModel>) {
          final data = state.dataObject;
          
          FirebaseAnalytics().logEvent(name: 'appointment_event',parameters:null);
          if (data == null || data.appointDate == null) {
            // No hay citas, toca crear una
            return Center(
              child: Text(
                'No se han programado citas.',
                style: kMediumTextStyle.copyWith(
                  color: kPrimaryDarkGrey,
                ),
              ),
            );
          }
          idAp = data.id;
          final date = DateTime.parse(data.appointDate) ?? DateTime.now();
          var typeText = '-';
          switch ('${data.state}') {
            case '1':
              typeText = 'Agendada';
              break;
            case '0':
              typeText = 'Cancelada';
              break;
            case '2':
              typeText = 'Reprogramada';
              break;
          }

          return Column(
            children: [
              CustomScrollView(
                slivers: [
                  /// * Contenido
                  _ProximaCitaWidget(
                    appointment: data,
                    typeText: typeText,
                    date: date,
                  ),

                  SliverPadding(
                    padding: const EdgeInsets.all(16),
                    sliver: SliverToBoxAdapter(
                      child: InkWell(
                        onTap: () => _selectDate(context),
                        child: ReusableCard(
                          backColor: const Color(0xFF675F73),
                          marginAll: 0,
                          height: 40,
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              Text(
                                'Reagendar cita',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: KbackGroundColor,
                                  fontSize: 18,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),

                  SliverPadding(
                    padding: const EdgeInsets.all(16),
                    sliver: SliverToBoxAdapter(
                      child: InkWell(
                        onTap: ()=>widget.cancelAppointment(idAp),
                        child: const Text(
                          'Cancelar cita',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: kPrimaryDarkGrey,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          );
        } else {
          return Column(
            children: const [
              Text(
                'Vuelve a intentar',
              ),
            ],
          );
        }
      },
    );
  }
}

// * Próxima cita
class _ProximaCitaWidget extends StatelessWidget {
  _ProximaCitaWidget({
    @required this.appointment,
    @required this.typeText,
    @required this.date,
  });

  final DataAppointmentModel appointment;
  final String typeText;
  final DateTime date;
  final DateFormat dateFormat = DateFormat('MMM d, y');

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      sliver: SliverToBoxAdapter(
        child: ShrinkWrappingViewport(
          offset: ViewportOffset.zero(),
          slivers: [
            const SliverToBoxAdapter(
              child: SizedBox(
                height: 24,
              ),
            ),

            /// * Próxima Cita
            const SliverToBoxAdapter(
              child: Text(
                'Próxima cita',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),

            const SliverToBoxAdapter(
              child: SizedBox(
                height: 8,
              ),
            ),

            SliverPadding(
              padding: const EdgeInsets.only(bottom: 8),
              sliver: SliverToBoxAdapter(
                child: ReusableCard(
                  backColor: const Color(0xFF675F73),
                  marginAll: 0,
                  height: 200,
                  children: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        dateFormat.format(date).toString(),
                        overflow: TextOverflow.fade,
                        style: kBigTextStyle.copyWith(
                          fontSize: 48,
                        ),
                      ),
                      Text(
                        '$typeText - ${appointment.type}',
                        overflow: TextOverflow.fade,
                        style: const TextStyle(
                          color: KbackGroundColor,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        appointment.doctorName,
                        overflow: TextOverflow.fade,
                        style: const TextStyle(
                          color: KbackGroundColor,
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// * Registro de citas
class _AppointmentRegister extends StatefulWidget {
  const _AppointmentRegister({
    @required this.togglePage,
    @required this.userId,
  });

  final Function() togglePage;
  final String userId;

  @override
  __AppointmentRegisterState createState() => __AppointmentRegisterState();
}

class __AppointmentRegisterState extends State<_AppointmentRegister> {
  DateTime selectedDate = DateTime.now();
  final doctorCtr = TextEditingController(text: '');
  final typeCtr = TextEditingController(text: '');
  final DateFormat dateFormat = DateFormat('MMM d, y');

  void save() {
    //
    if (doctorCtr.text.isEmpty || typeCtr.text.isEmpty) {
      Scaffold.of(context).hideCurrentSnackBar();
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text(
            'Faltan campos por llenar.',
            style: kMediumTextStyle.copyWith(
              color: Colors.white,
            ),
          ),
          backgroundColor: kPrimaryDarkGrey,
        ),
      );
      return;
    }

    final appointment = DataAppointmentModel(
      appointDate: selectedDate.toString(),
      createdDate: DateTime.now().toString(),
      state: 1,
      doctorName: doctorCtr.text.trim(),
      modifyDate: '',
      type: typeCtr.text.trim(),
    );

    BlocProvider.of<CrudBloc<DataAppointmentModel>>(context).add(
      CrudRequestedEvent<DataAppointmentModel>(
        api: 'appointment/${widget.userId}',
        dataParser: const DataAppointmentParser(),
        requestType: RequestType.postRequest,
        body: <String, dynamic>{
          'ApointDate': appointment.appointDate,
          'CreatDate': appointment.createdDate,
          'DoctorName': appointment.doctorName,
          'ModifyDate': appointment.modifyDate,
          'Estate': appointment.state,
          'Type': appointment.type,
        },
      ),
    );

    widget.togglePage();
  }

  Future<void> _selectDate(BuildContext context) async {
    final picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: SafeArea(
        top: false,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // * Inicar sesion
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 4),
                child: Text(
                  'Registrar cita',
                  textAlign: TextAlign.center,
                  style: kMediumTextStyle.copyWith(color: kPrimaryDarkGrey),
                ),
              ),
              const SizedBox(
                height: 32,
              ),
              // *
              Text(
                'Fecha de la cita: ${dateFormat.format(selectedDate)}',
                style: kMediumTextStyle.copyWith(
                  color: kPrimaryDarkGrey,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              ElevatedButton(
                onPressed: () => _selectDate(context),
                style: ElevatedButton.styleFrom(
                  primary: const Color(0xFF675F73),
                ),
                child: const Text('Selecciona una fecha'),
              ),

              const SizedBox(
                height: 12,
              ),
              // *
              Text(
                'Motivo (*)',
                style: kMediumTextStyle.copyWith(
                  color: kPrimaryDarkGrey,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              _InputFieldWidget(
                onChange: (val) {},
                label: 'Escribe aquí',
                controlador: typeCtr,
              ),
              const SizedBox(
                height: 12,
              ),
              // *
              Text(
                'Nombre del doctor (*)',
                style: kMediumTextStyle.copyWith(
                  color: kPrimaryDarkGrey,
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              _InputFieldWidget(
                onChange: (val) {},
                label: 'Escribe aquí',
                controlador: doctorCtr,
              ),
              const SizedBox(
                height: 32,
              ),
              // *

              InkWell(
                onTap: save,
                child: ReusableCard(
                  backColor: const Color(0xFF675F73),
                  marginAll: 0,
                  height: 40,
                  children: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text(
                        'Agendar cita',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: KbackGroundColor,
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// * Input fields
class _InputFieldWidget extends StatelessWidget {
  const _InputFieldWidget({
    @required this.label,
    this.onChange,
    this.controlador,
    Key key,
  }) : super(key: key);

  final void Function(String) onChange;
  final String label;
  final TextEditingController controlador;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(
          10,
        ),
        border: Border.all(
          color: kligtgrey,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: TextFormField(
          cursorColor: kPrimaryDarkGrey,
          maxLength: 25,
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: label,
            hintStyle: kMediumTextStyle.copyWith(
              color: kligtgrey,
            ),
            counterText: '',
          ),
          style: kMediumTextStyle.copyWith(color: kPrimaryDarkGrey),
          onChanged: onChange,
          controller: controlador,
        ),
      ),
    );
  }
}
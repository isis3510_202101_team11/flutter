import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:minihomelab/Components/LodingScreen.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/components/Reusable_Card.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/components/Resusable_Button.dart';
import 'SingUp_screen.dart';
import 'Home_Page.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:minihomelab/Components/PopUpMenu.dart';
import 'dart:async';


class WelcomeScreen extends StatefulWidget {
  static const String id = 'welcome_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  String errorMesage = "";
  String email;
  String password;
  bool keyboard = false;
  bool showSpinner = false;
  bool autolonding = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      autolonding = true;
    });
    LoginController.instance.CheckConectivity().then((value) {
      if (value) {
        LoginController.instance.authoLog().then((value) {
          if (value) {
            LoginController.instance.getUser().then((user) {
              if (user != null) {
                ProgressionController.instance
                    .getUserMH(LoginController.currentUser.userId)
                    .then((medicalHistory) {
                  if (medicalHistory != null) {
                    Navigator.popAndPushNamed(context, HomePage.id);
                  } else {
                    setState(() {
                      autolonding = false;
                    });
                  }
                });
              } else {
                setState(() {
                  autolonding = false;
                });
              }
            });
          } else {
            setState(() {
              autolonding = false;
            });
          }
        });
      } else {
        PopUpMenu pop = PopUpMenu();
        pop.showDialogPopUp(
            "Sin conexion",
            "En este momento no posee una conexion trata de ingresar nuevamente",
            context);
        setState(() {
          autolonding = false;
        });
      }
    });

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        setState(() {
          keyboard = visible;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return autolonding == true
        ? LodingWidget()
        : Scaffold(
            body: ModalProgressHUD(
              inAsyncCall: showSpinner,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SafeArea(
                    child: Text(
                      "Mini-Home Lab",
                      style: kTitleText.copyWith(
                          fontSize: keyboard == true ? 20 : 40),
                    ),
                  ),
                  Hero(
                    tag: "iconh",
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
                      child: Icon(
                        Icons.medical_services_outlined,
                        color: Color(0xFF3E4259),
                        size: keyboard == true ? 20 : 80,
                      ),
                    ),
                  ),
                  Text(
                    errorMesage,
                    style: kTitleText.copyWith(
                        color: Colors.red.shade700,
                        fontSize: keyboard == true ? 17 : 20),
                  ),
                  Center(
                    child: ReusableCard(
                      backColor: Color(0xFF3E4259),
                      height: keyboard == true ? 260 : 320.0,
                      children: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          SizedBox(),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: TextField(
                              keyboardType: TextInputType.emailAddress,
                              textAlign: TextAlign.center,
                              decoration: kTextFieldDecoration.copyWith(
                                  hintText: "Correo"),
                              onChanged: (value) {
                                email = value;
                              },
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                            child: TextField(
                              obscureText: true,
                              keyboardType: TextInputType.emailAddress,
                              textAlign: TextAlign.center,
                              decoration: kTextFieldDecoration.copyWith(
                                  hintText: "Clave"),
                              onChanged: (value) {
                                password = value;
                              },
                            ),
                          ),
                          Hero(
                            tag: "LoginB",
                            child: Resusable_Button(
                              text: "Ingresar",
                              textSize: 18,
                              onPress: () async {
                                try {
                                  setState(() {
                                    showSpinner = true;
                                  });

                                  LoginController.instance
                                      .CheckConectivity()
                                      .then((value) {
                                    if (value) {
                                      LoginController.instance
                                          .tryConnection()
                                          .then((value) {
                                        if (value == 200) {
                                          LoginController.instance
                                              .loginUser(email, password)
                                              .then((value) {
                                            if (value[0] == true) {
                                              LoginController.instance
                                                  .getUser()
                                                  .then((value) {
                                                if (value != null) {
                                                  ProgressionController.instance
                                                      .getUserMH(LoginController
                                                          .currentUser.userId)
                                                      .then((value) {
                                                    if (value != null) {
                                                      setState(() {
                                                        showSpinner = false;
                                                      });
                                                      Navigator.popAndPushNamed(
                                                          context, HomePage.id);
                                                    }
                                                  });
                                                } else {
                                                  PopUpMenu pop = PopUpMenu();
                                                  pop.showDialogPopUp(
                                                      "Error en la conexión",
                                                      "No fue posible ingresar, intente mas tarde",
                                                      context);
                                                  setState(() {
                                                    showSpinner = false;
                                                  });
                                                }
                                              });
                                            } else {
                                              setState(() {
                                                errorMesage = value[1];
                                                showSpinner = false;
                                              });
                                            }
                                          });
                                        } else {
                                          PopUpMenu pop = PopUpMenu();
                                          pop.showDialogPopUp(
                                              "El servidor No responde",
                                              "En este momento nuestro servidor no responde, trate mas tarde ",
                                              context);
                                          setState(() {
                                            showSpinner = false;
                                          });
                                        }
                                      });
                                    } else {
                                      PopUpMenu pop = PopUpMenu();
                                      pop.showDialogPopUp(
                                          "Sin Conexion a internet",
                                          "En este momento no posees una conexión a internet",
                                          context);
                                      setState(() {
                                        showSpinner = false;
                                      });
                                    }
                                  });
                                } on TimeoutException catch (e) {
                                  throw "TimeOut Exeption";
                                }
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 100.0),
                            height: keyboard == true ? 40 : 50.0,
                            child: TextButton(
                              onPressed: () {
                                Navigator.pushNamed(context, SignUpScreen.id);
                              },
                              child: Text(
                                "Registrarme",
                                style: kMediumTextStyle.copyWith(
                                    fontSize: keyboard == true ? 20 : 25),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}



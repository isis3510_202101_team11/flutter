import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';

class MedicalHistory {
  int id;
  String UserId;
  String eps;
  bool Finalizo;
  String afliccion;
  int estado;
  DateTime Finalizacion;
  DateTime Cumplimientos;
  DateTime Incio;
  DateTime CreateTime;

  MedicalHistory(
      {this.id,
      this.eps = "",
      this.Finalizo = false,
      this.afliccion = "",
      this.estado = 1,
      @required this.Finalizacion,
      @required this.Incio,
      this.Cumplimientos,
      this.CreateTime});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DataBaseProvider.COLUMN_UserID: UserId,
      DataBaseProvider.COLUMN_EPS: eps,
      DataBaseProvider.COLUMN_FECHACUMPLMIENTO:
          Cumplimientos.millisecondsSinceEpoch,
      DataBaseProvider.COLUMN_FINALIZO: Finalizo == true ? 1 : 0,
      DataBaseProvider.COLUMN_AFLICCION: afliccion,
      DataBaseProvider.COLUMN_ESTADO: estado,
      DataBaseProvider.COLUMN_FECHAFINALIZACION:
          Finalizacion.millisecondsSinceEpoch,
      DataBaseProvider.COLUMN_INICIO: Incio.millisecondsSinceEpoch,
      DataBaseProvider.COLUMN_CREATETIME: CreateTime.millisecondsSinceEpoch
    };
    if (id != null) {
      map[DataBaseProvider.COLUMN_ID_MH] = id;
    }
    return map;
  }

  MedicalHistory.fromMap(Map<String, dynamic> map) {
    id = map[DataBaseProvider.COLUMN_ID];
    UserId = map[DataBaseProvider.COLUMN_UserID];
    eps = map[DataBaseProvider.COLUMN_EPS];
    Cumplimientos = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.COLUMN_FECHACUMPLMIENTO]);
    Finalizo = map[DataBaseProvider.COLUMN_FINALIZO] == 1 ? true : false;
    afliccion = map[DataBaseProvider.COLUMN_AFLICCION];
    estado = map[DataBaseProvider.COLUMN_ESTADO];
    Finalizacion = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.COLUMN_FECHAFINALIZACION]);
    Incio = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.COLUMN_INICIO]);
    CreateTime = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.COLUMN_CREATETIME]);
  }
}

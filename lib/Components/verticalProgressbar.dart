import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:minihomelab/Constanse.dart';

class VerticalProgressByDay extends StatefulWidget {
  final String dayLeter;
  final double porcentageFill;
  VerticalProgressByDay({this.dayLeter = "l", this.porcentageFill = 0.5});

  @override
  _VerticalProgressByDayState createState() => _VerticalProgressByDayState();
}

class _VerticalProgressByDayState extends State<VerticalProgressByDay> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RotatedBox(
          quarterTurns: -1,
          child: LinearPercentIndicator(
            width: 100.0,
            lineHeight: 12.0,
            percent: widget.porcentageFill,
            backgroundColor: KbackGroundColor,
            progressColor: kDarkPink,
          ),
        ),
        Text(
          widget.dayLeter.toUpperCase(),
          style: kMediumTextStyle.copyWith(fontSize: 10.0),
        ),
      ],
    );
  }


}

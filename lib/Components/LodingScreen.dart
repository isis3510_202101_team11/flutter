

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Constanse.dart';

class LodingWidget extends StatelessWidget {
  const LodingWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          height: 150,
        ),
        Hero(
          tag: "iconh",
          child: Padding(
            padding:
            EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
            child: Icon(
              Icons.medical_services_outlined,
              color: Color(0xFF3E4259),
              size: 100,
            ),
          ),
        ),
        Text(
          "Mini-Home Lab",
          style: kTitleText.copyWith(
              fontSize: 30),
        ),
        Center(child: SizedBox(
          height: 80,
          width: 80,
          child: CircularProgressIndicator(
            semanticsValue: "Cargando",
          ),
        )),
        SizedBox(
          height: 150,
        ),
      ],
    ),);
  }
}
import 'dart:async';
import 'dart:convert';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';
import 'package:http/http.dart' as http;
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:minihomelab/models/Medical_HistoryModel.dart';
import 'dart:math';
import 'package:minihomelab/models/Exercises_Model.dart';

class ProgressionController {
  final String MedicalHistoryURL = urlLocal + "medicalHistory/";
  final String ExerciseURL = urlLocal + "exercises/Rotine/";
  static bool askForState = true;
  static MedicalHistory mh;
  static List<exercises> exer;
  static exercises currentExercise;
  final eps = ["Unisalud", "Mermedica", "Cafesalud", "Salud Total", "Sanitas"];
  final afliccionlist = [
    "espalda",
    "muñeca",
    "pie",
    "talon",
    "rodilla",
    "cadera",
    "hombro"
  ];

  ProgressionController._privateConstructor();

  static final ProgressionController _instance =
      ProgressionController._privateConstructor();

  static ProgressionController get instance => _instance;

  Future<MedicalHistory> getUserMH(String uid) async {
    try {
      mh = null;
      mh = await DataBaseProvider.instance.getMh(uid);
      if (mh == null) {
        final res = await http
            .get(Uri.parse(MedicalHistoryURL + uid))
            .timeout(const Duration(seconds: 8));
        if (res.statusCode == 200) {
          final body = jsonDecode(res.body);
          mh = MedicalHistory(
            eps: body["EPS"] as String,
            Finalizo: body["Finalizo"],
            afliccion: body["afliccion"],
            Finalizacion: DateTime.fromMillisecondsSinceEpoch(
                (body["fecha_finalizacion"]["_seconds"]) * 1000),
            Cumplimientos: DateTime.fromMillisecondsSinceEpoch(
                (body["FechaDeCumplimento"]["_seconds"]) * 1000),
            Incio: DateTime.fromMillisecondsSinceEpoch(
                (body["inicio"]["_seconds"]) * 1000),
            CreateTime: DateTime.now(),
          );
          mh.UserId = uid;
          ProgressionController.mh = mh;
          await DataBaseProvider.instance.insertMh(mh);
          return mh;
        } else {}
      } else {
        return mh;
      }
    } catch (e) {
      throw e.toString();
    }
  }

  Future updateState(int state) async {
    ProgressionController.mh.estado = state;
    await DataBaseProvider.instance.updateMh(ProgressionController.mh);
    return await http.put(
        Uri.parse(MedicalHistoryURL + LoginController.currentUser.userId),
        body: {"estado": state.toString()});
  }

  Future RegisterMh(String userId, int alternative) async {
    DateTime hoy = DateTime.now();
    final rng = new Random();
    String alter;
    switch (alternative) {
      case 10:
        {
          alter = "Niguno";
        }
        break;
      case 9:
        {
          alter = "Acupuntura";
        }
        break;
      case 8:
        {
          alter = "Aterterapia";
        }
        break;
      case 7:
        {
          alter = "Hidroterapia";
        }
        break;
      case 6:
        {
          alter = "Hipoterapia";
        }
        break;
      case 5:
        {
          alter = "Musicoterapia";
        }
        break;
      case 4:
        {
          alter = "Psicodanza";
        }
        break;
      case 3:
        {
          alter = "Risoterapia";
        }
        break;
      case 2:
        {
          alter = "Ultrasonido";
        }
        break;
      case 1:
        {
          alter = "Homeopatía";
        }
        break;
    }
    final res = await http.post(Uri.parse(MedicalHistoryURL + userId), body: {
      "EPS": eps[rng.nextInt(eps.length - 1)],
      "FechaDeCumplimento": hoy.toString(),
      "Finalizo": false.toString(),
      "afliccion": afliccionlist[rng.nextInt(afliccionlist.length - 1)],
      "fecha_finalizacion":
          new DateTime(hoy.year, hoy.month + 1, hoy.day).toString(),
      "estado": 5.toString(),
      "inicio": hoy.toString(),
      "alternativa": alter,
    });

    return res;
  }

  Future<List<exercises>> GetDayExercises(String UserId) async {
    try {
      exer = [];

      exer = await DataBaseProvider.instance.getExercise(UserId);

      if (exer.length == 0) {
        final res = await http
            .get(Uri.parse(ExerciseURL + UserId))
            .timeout(const Duration(seconds: 8));
        if (res.statusCode == 200) {
          final body = jsonDecode(res.body);
          final len = body.length;
          var j;
          for (var i = 0; i < len; i++) {
            j = body[i];
            exercises ex = exercises(
              name: j["Name"],
              userId: LoginController.currentUser.userId,
              ExerId: j["ExerciseId"],
              NumRepeticions: j["NumRepeticions"] as int,
              NumSeries: j["NumSeries"] as int,
              ResX: j["Restriccions"]["x"] as int,
              ResY: j["Restriccions"]["y"] as int,
              ResZ: j["Restriccions"]["z"] as int,
              time: j["Tiempo"] as int,
              Create: DateTime.now(),
            );

            if (j["Explicacion"] != null) {
              ex.Explicacion = j["Explicacion"];
            }
            if (j["imgSource"] != null) {
              ex.URL = j["imgSource"];
            }

            exer.add(ex);
            await DataBaseProvider.instance.insertExercise(exer[i]);
          }
          return exer;
        }
      } else {
        return exer;
      }
    } catch (error) {
      return [];
    }
  }

  Future<double> GetCompleteExercises(String UserId) async {
    exer = [];
    double porcent = 0;
    exer = await DataBaseProvider.instance.getExercise(UserId);
    if (exer.length == 0) {
      return 0;
    } else {
      for (int i = 0; i < exer.length; i++) {
        if (exer[i].isDone) {
          porcent++;
        }
      }
      return porcent / exer.length;
    }
  }
}

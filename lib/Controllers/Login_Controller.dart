import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:connectivity/connectivity.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/models/userData_Model.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:shared_preferences/shared_preferences.dart';

class LoginController {
  final firebase_storage.FirebaseStorage fb =
      firebase_storage.FirebaseStorage.instance;

  static String userId = "";
  static userData currentUser;
  final String profileUrl = urlLocal + "profiles/";
  final String CheckUrl = urlLocal;
  final picker = ImagePicker();

  LoginController._privateConstructor();

  static final LoginController _instance =
      LoginController._privateConstructor();

  static LoginController get instance => _instance;

  static final _auth = FirebaseAuth.instance;

  Future<List> registerUser(
      String email, String password, String passwordConfirm) async {
    if (password.isEmpty) {
      return [false, "No sé a ingresado una contraseña"];
    } else if (password != passwordConfirm) {
      return [false, "La contraseña no Coincide"];
    } else if (email.isEmpty) {
      return [false, "No se a ingresado ningún Correo"];
    } else {
      try {
        final newUser = await _auth.createUserWithEmailAndPassword(
            email: email, password: password);
        if (newUser != null) {
          userId = _auth.currentUser.uid;

          return [true, ""];
        } else {
          return [false, ""];
        }
      } on FirebaseAuthException catch (e) {
        return [false, e.code];
      }
    }
  }

  Future addUserData(String name, DateTime age) async {
    final res = await http.post(Uri.parse(profileUrl), body: {
      "uId": userId,
      "Name": name,
      "FechaNacimiento": age.toString(),
      "ProfilePic": "",
      "lastLogin": DateTime.now().toString()
    });
    return res;
  }

  Future<bool> authoLog() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String email = prefs.getString("email");
    if (email != "" && email != null) {
      String password = prefs.getString("password");
      final user = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      if (user != null) {
        userId = _auth.currentUser.uid;
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  Future<List> loginUser(String email, String password) async {
    try {
      if (email == null || password == null) {
        return [false, "Ingrese la información requerida"];
      }

      final user = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      if (user != null) {
        userId = _auth.currentUser.uid;
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("email", email);
        prefs.setString("password", password);

        return [true, ""];
      } else {
        return [false, "Error"];
      }
    } on FirebaseAuthException catch (e) {
      return [false, e.code];
    }
  }

  Future<bool> CheckConectivity() async {
    final resutl = await Connectivity().checkConnectivity();
    return resutl != ConnectivityResult.none;
  }

  Future<void> logOutFromApp() async {
    userId = "";
    ProgressionController.askForState = true;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("email", "");
    prefs.setString("password", "");
    prefs.setString("LastLoginDate", DateTime.utc(1989, 11, 9).toString());

    return _auth.signOut();
  }

  Future<userData> getUser() async {
    try {
      LoginController.currentUser =
          await DataBaseProvider.instance.getUser(userId);
      if (LoginController.currentUser == null) {
        final res = await http
            .get(Uri.parse(profileUrl + userId))
            .timeout(const Duration(seconds: 8));

        if (res.statusCode == 200) {
          final body = jsonDecode(res.body);
          LoginController.currentUser = userData(
              userId: userId,
              // age: body["Edad"] as int,
              birthday: DateTime.fromMillisecondsSinceEpoch(
                  (body["FechaNacimiento"]["_seconds"]) * 1000),
              name: body["Name"] as String,
              lastLogin: DateTime.fromMillisecondsSinceEpoch(
                  (body["lastLogin"]["_seconds"]) * 1000),
              profilePick: "ProfilePictures/" + body["ProfilePic"] as String,
              ProfilepickUrl: "");

          if (LoginController.currentUser.profilePick != "ProfilePictures/") {
            String image =
                await getImage(LoginController.currentUser.profilePick);
            LoginController.currentUser.ProfilepickUrl = image;
          }

          await DataBaseProvider.instance
              .insertUser(LoginController.currentUser);
          return LoginController.currentUser;
        } else {
          throw "Cant get userdata";
        }
      } else {
        return LoginController.currentUser;
      }
    } on TimeoutException catch (e) {
      throw "TimeOut expetion";
    } catch (e) {
      throw e.toString();
    }
  }

  Future<String> getImage(String url) async {
    try {
      return await fb.ref().child(url).getDownloadURL();
    } catch (e) {
      return "";
    }
  }

  Future<File> chooseImage() async {
    final pickfile = await picker.getImage(source: ImageSource.gallery);
    return File(pickfile.path);
  }

  Future uploadPicture(File photo) async {
    String name = LoginController.currentUser.userId + "ProfilePicture.jpg";
    final snapshot =
        await fb.ref().child("ProfilePictures/" + name).putFile(photo);
    LoginController.currentUser.profilePick = name;
    final newUrl = await snapshot.ref.getDownloadURL();
    LoginController.currentUser.ProfilepickUrl = newUrl;

    http.put(Uri.parse(profileUrl + userId), body: {
      "ProfilePic": name,
    });

    return await DataBaseProvider.instance
        .updateUser(LoginController.currentUser);
  }

  Future<dynamic> tryConnection() async {
    try {
      final resp = await http
          .get(Uri.parse(CheckUrl))
          .timeout(const Duration(seconds: 5));
      return resp.statusCode;
    } catch (e) {
      return e;
    }
  }

  Future<String> UpdateLoginTime() async {
    return DateTime.now().toString();
  }
}

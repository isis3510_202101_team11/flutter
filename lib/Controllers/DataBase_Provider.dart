import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:minihomelab/models/Exercises_Model.dart';
import 'package:minihomelab/models/Medical_HistoryModel.dart';
import 'package:minihomelab/models/userData_Model.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:path/path.dart';

class DataBaseProvider {
  DataBaseProvider._privateConstructor();

  static const String TABLE_USERDATA = "UserData";
  static const String COLUMN_ID = "id";
  static const String COLUMN_UserID = "userId";
  static const String COLUMN_NAME = "name";
  static const String COLUMN_BIRTHDAY = "birthday";
  static const String COLUMN_PROFILEPICTURE = "profilePicture";
  static const String COLUMN_PICURL = "pictureURL";
  static const String COLUMN_LASTLOGIN = "lastLogin";

  static const String TABLE_MH = "MedicalHistryData";
  static const String COLUMN_ID_MH = "id ";
  static const String COLUMN_EPS = "eps";
  static const String COLUMN_FECHACUMPLMIENTO = "fechacumplimiento";
  static const String COLUMN_FINALIZO = "finalizo";
  static const String COLUMN_AFLICCION = "afliccion";
  static const String COLUMN_ESTADO = "estado";
  static const String COLUMN_FECHAFINALIZACION = "fechafinalizacion";
  static const String COLUMN_INICIO = "inicio";
  static const String COLUMN_CREATETIME = "CreateTime";

  static const String TABLE_EXERC = "ExerciseData";

  static const String TABLE_EX_DIFICULTAD = "dificultad";
  static const String TABLE_EX_NUMREP = "repeticiones";
  static const String TABLE_EX_NUMSER = "series";
  static const String COLUMN_ExId = "ExerciseId";
  static const String TABLE_EX_X = "restricionX";
  static const String TABLE_EX_Y = "restricionY";
  static const String TABLE_EX_Z = "restricionZ";
  static const String TABLE_EX_TIME = "tiempo";
  static const String TABLE_EX_ISDONE = "Done";
  static const String CREATE = "CreateTime";
  static const String IMAGEURL = "ImageURL";
  static const String EXPLICACION = "Explicacion";
  static final DataBaseProvider _instance =
      DataBaseProvider._privateConstructor();

  static DataBaseProvider get instance => _instance;

  Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await createDatabase();
      return _database;
    }
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();
    return await openDatabase(
      join(dbPath, "MiniLab5.db"),
      version: 6,
      onCreate: (Database database4, int version) async {
        await database4.execute(
          "CREATE TABLE $TABLE_USERDATA ("
          "$COLUMN_ID INTEGER PRIMARY KEY,"
          "$COLUMN_UserID TEXT,"
          "$COLUMN_NAME TEXT,"
          "$COLUMN_BIRTHDAY INTEGER,"
          "$COLUMN_PROFILEPICTURE TEXT,"
          "$COLUMN_PICURL TEXT,"
          "$COLUMN_LASTLOGIN INTEGER"
          ")",
        );

        await database4.execute(
          "CREATE TABLE $TABLE_MH ("
          "$COLUMN_ID_MH INTEGER PRIMARY KEY,"
          "$COLUMN_UserID TEXT,"
          "$COLUMN_EPS TEXT,"
          "$COLUMN_FECHACUMPLMIENTO INTEGER,"
          "$COLUMN_FINALIZO INTEGER,"
          "$COLUMN_AFLICCION TEXT,"
          "$COLUMN_ESTADO INTEGER,"
          "$COLUMN_FECHAFINALIZACION INTEGER,"
          "$COLUMN_INICIO INTEGER,"
          "$COLUMN_CREATETIME INTEGER"
          ")",
        );

        await database4.execute(
          "CREATE TABLE $TABLE_EXERC ("
          "$COLUMN_ID INTEGER PRIMARY KEY,"
          "$COLUMN_UserID TEXT,"
          "$COLUMN_NAME TEXT,"
          "$COLUMN_ExId TEXT,"
          "$TABLE_EX_NUMREP INTEGER,"
          "$TABLE_EX_NUMSER INTEGER,"
          "$TABLE_EX_X INTEGER,"
          "$TABLE_EX_Y INTEGER,"
          "$TABLE_EX_Z INTEGER,"
          "$TABLE_EX_TIME INTEGER,"
          "$TABLE_EX_ISDONE INTEGER,"
          "$CREATE INTEGER,"
          "$IMAGEURL TEXT,"
          "$EXPLICACION text"
          ")",
        );
      },
    );
  }

  Future<userData> insertUser(userData newuser) async {
    final db = await database;
    newuser.id = await db.insert(TABLE_USERDATA, newuser.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return newuser;
  }

  Future<MedicalHistory> insertMh(MedicalHistory newmh) async {
    final db = await database;

    newmh.id = await db.insert(TABLE_MH, newmh.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return newmh;
  }

  Future<exercises> insertExercise(exercises newex) async {
    final db = await database;

    newex.id = await db.insert(TABLE_EXERC, newex.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);

    return newex;
  }

  Future<userData> getUser(String UserId) async {
    final db = await database;
    final data = await db.query(TABLE_USERDATA,
        columns: [
          COLUMN_ID,
          COLUMN_UserID,
          COLUMN_NAME,
          COLUMN_BIRTHDAY,
          COLUMN_PROFILEPICTURE,
          COLUMN_PICURL,
          COLUMN_LASTLOGIN
        ],
        where: "$COLUMN_UserID = ?",
        whereArgs: [UserId]);

    userData user;
    data.forEach((currentUser) {
      user = userData.fromMap(currentUser);
    });

    return user;
  }

  Future<MedicalHistory> getMh(String UserId) async {
    final db = await database;
    final data = await db.query(TABLE_MH,
        columns: [
          COLUMN_ID_MH,
          COLUMN_UserID,
          COLUMN_EPS,
          COLUMN_FECHACUMPLMIENTO,
          COLUMN_FINALIZO,
          COLUMN_AFLICCION,
          COLUMN_ESTADO,
          COLUMN_FECHAFINALIZACION,
          COLUMN_INICIO,
          COLUMN_CREATETIME
        ],
        where: "$COLUMN_UserID = ?",
        whereArgs: [UserId]);
    MedicalHistory user;
    data.forEach((currentUser) {
      user = MedicalHistory.fromMap(currentUser);
    });

    return user;
  }

  Future<List<exercises>> getExercise(String UserId) async {
    final db = await database;
    final data = await db.query(TABLE_EXERC,
        columns: [
          COLUMN_ID,
          COLUMN_UserID,
          COLUMN_NAME,
          COLUMN_ExId,
          TABLE_EX_NUMREP,
          TABLE_EX_NUMSER,
          TABLE_EX_X,
          TABLE_EX_Y,
          TABLE_EX_Z,
          TABLE_EX_TIME,
          TABLE_EX_ISDONE,
          CREATE,
          IMAGEURL,
          EXPLICACION
        ],
        where: "$COLUMN_UserID = ?",
        whereArgs: [UserId]);
    exercises user;
    List<exercises> exlist = [];
    data.forEach((currentUser) {
      user = exercises.fromMap(currentUser);

      exlist.add(user);
    });
    if (exlist.length > 0) {
      DateTime date = exlist[0].Create;
      DateTime hoy = DateTime.now();
      if (date.year != hoy.year &&
          date.month != hoy.month &&
          date.day != hoy.day) {
        exlist = [];

        await db.delete(TABLE_EXERC,
            where: "$COLUMN_UserID = ?", whereArgs: [UserId]);
      }
    }

    return exlist;
  }

  Future<int> updateUser(userData user) async {
    final db = await database;

    return await db.update(TABLE_USERDATA, user.toMap(),
        where: "$COLUMN_ID = ?", whereArgs: [user.id]);
  }

  Future<int> updateMh(MedicalHistory mh) async {
    final db = await database;

    return await db.update(TABLE_MH, mh.toMap(),
        where: "$COLUMN_ID = ?", whereArgs: [mh.id]);
  }

  deleteAllExerExer() async {
    final db = await database;
    await db.delete(TABLE_EXERC);
  }

  deleteExer() async {
    final db = await database;
    await db.delete(TABLE_EXERC,
        where: "$COLUMN_UserID = ?", whereArgs: [LoginController.userId]);
  }

  Future<int> updateExer(exercises excer) async {
    final db = await database;

    return await db.update(TABLE_EXERC, excer.toMap(),
        where: "$COLUMN_ID = ?", whereArgs: [excer.id]);
  }
}

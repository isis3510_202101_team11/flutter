import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';

class PopUpMenu {
  String popUpTitle;
  String bodyText;

  showDialogPopUp(title, text, context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Center(
              child: Text(
            title,
            textAlign: TextAlign.center,
          )),
          content: Container(
            height: 150,
            child: Column(
              children: [
                Text(
                  text,
                ),
                SizedBox(
                  height: 45,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Center(
                    child: Text(
                      "Ok",
                      style: kMediumTextStyle,
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                    primary: DarkPurple,
                  ),
                ),
              ],
            ),
          ),
          elevation: 10,
          backgroundColor: KbackGroundColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        );
      },
    );
  }
}

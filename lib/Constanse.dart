import 'package:flutter/material.dart';

const Color KbackGroundColor = Color(0xFFF2F2F0);

const Color DarkPurple = Color(0xFF3E4259);
const Color LightPink = Color(0xFFD9B7B0);

const Color kligtgrey = Color(0xFF675F73);
const Color kDarkPink = Color(0xFFBF8484);
const Color kPrimaryDarkGrey = Color(0xFF3E4259);
const TextStyle kMediumTextStyle = TextStyle(
  fontSize: 20.0,
  color: KbackGroundColor,
);
const TextStyle kBigTextStyle = TextStyle(
  fontSize: 40.0,
  color: KbackGroundColor,
  fontWeight: FontWeight.bold,
);
//Cambiar esta para correr local
const String urlLocal = 'https://mini-home-lab.herokuapp.com/';

const TextStyle kTitleText = TextStyle(
  fontSize: 40.0,
  color: kligtgrey,
  fontWeight: FontWeight.bold,
);
const kTextFieldDecoration = InputDecoration(
  fillColor: Colors.white,
  hintText: 'Enter a value',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF3E4259), width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF3E4259), width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
  ),
  filled: true,
);

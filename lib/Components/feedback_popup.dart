import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../Constanse.dart';

class FeedbackPopup extends StatefulWidget {
  final bool completed;
  final Function onSubmit;

  FeedbackPopup({Key key, this.completed, this.onSubmit}) : super(key: key);

  @override
  _FeedbackPopupState createState() =>
      _FeedbackPopupState(completed: completed, onSubmit: onSubmit);
}

class _FeedbackPopupState extends State<FeedbackPopup> {
  final bool completed;
  final Function onSubmit;

  int feedback;
  bool submittable = false;

  _FeedbackPopupState({this.completed, this.onSubmit});

  void onChanged(String value) {
    final int number = int.tryParse(value);

    feedback = number;

    setState(() {
      submittable = (number != null && number >= 1 && number <= 10);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      elevation: 16,
      child: Container(
        height: 400,
        width: 360,
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            if (completed)
              Text(
                "🥳¡Felicitaciones! Has completado tu sesión 😊",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 24,
                  color: kDarkPink,
                  fontWeight: FontWeight.bold,
                ),
              ),
            Text(
              "Por favor, califica de 1 a 10 tu experiencia realizando este ejercicio.",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24,
                color: kDarkPink,
              ),
            ),
            TextField(
              onChanged: onChanged,
              keyboardType: TextInputType.number,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24,
                color: DarkPurple,
              ),
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ], // Only numbers can be entered
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                OutlinedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Omitir", style: TextStyle(color: DarkPurple))),
                if (submittable)
                  OutlinedButton(
                      onPressed: () {
                        onSubmit(feedback);
                        Navigator.of(context).pop();
                      },
                      child:
                          Text("Enviar", style: TextStyle(color: kDarkPink))),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minihomelab/Components/Exercise_popUp.dart';
import 'package:minihomelab/Components/LodingScreen.dart';
import 'package:minihomelab/Components/Resusable_Button.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Components/Reusable_Card.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/models/Exercises_Model.dart';
import 'package:minihomelab/screens/Profile_Screen.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class SequencesArguments {
  final int level ;

  SequencesArguments({
    @required this.level,
  });
}

class SequencesScreen extends StatefulWidget {
  static const String id = 'sequences_screen';

  @override
  _SequencesScreenState createState() => _SequencesScreenState();
}

class _SequencesScreenState extends State<SequencesScreen> {
  int state = 0;
  bool showProgress = false;
  @override
  Widget build(BuildContext context) {
    final args =
        ModalRoute.of(context).settings.arguments as SequencesArguments;

    return FutureBuilder<List<exercises>>(
      future: ProgressionController.instance
          .GetDayExercises(LoginController.currentUser.userId),
      builder: (BuildContext context, AsyncSnapshot<List<exercises>> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length == 0) {
            return Scaffold(
              appBar: AppBar(
                title: Text("Elige una secuencia"),
                centerTitle: true,
                actions: [
                  Padding(
                    padding:
                        EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
                    child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, ProfileCard.id);
                      },
                      child: Hero(
                        tag: "ProfilePic",
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: KbackGroundColor,
                          backgroundImage:
                              LoginController.currentUser.ProfilepickUrl == ""
                                  ? AssetImage("images/defaultprofile.jpg")
                                  : CachedNetworkImageProvider(
                                      LoginController
                                          .currentUser.ProfilepickUrl,
                                    ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: SafeArea(
                child: Center(
                  child: Text("Lo sentimos no hay ejercicios en este momento"),
                ),
              ),
            );
          }
          return ModalProgressHUD(
            inAsyncCall: showProgress,
            child: Scaffold(
              appBar: AppBar(
                title: Text("Elige una secuencia"),
                leading: Center(
                  child: TextButton(
                    child: Icon(
                      Icons.refresh_outlined,
                      color: KbackGroundColor,
                    ),
                    onPressed: () {
                      bool modal = true;
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text("Desea Generar Una Nueva Rutina?"),
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Center(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        ElevatedButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: Center(
                                            child: Text(
                                              "No",
                                              style: kMediumTextStyle,
                                            ),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                            primary: DarkPurple,
                                          ),
                                        ),
                                        ElevatedButton(

                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return AlertDialog(
                                                    title: Text(
                                                      "¿Como te sientes en este momento?",
                                                      style: kTitleText.copyWith(
                                                          fontSize: 25),
                                                    ),
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    10))),
                                                    content: Container(
                                                      height: 200,
                                                      child: Column(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceEvenly,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .stretch,
                                                        children: [
                                                          Expanded(
                                                            child:
                                                                CupertinoPicker(
                                                              itemExtent: 30,
                                                              useMagnifier: true,
                                                              backgroundColor:
                                                                  KbackGroundColor,
                                                              onSelectedItemChanged:
                                                                  (inte) {
                                                                state = 5 - inte;
                                                              },
                                                              children: [
                                                                Text("Muy bien"),
                                                                Text("Bien"),
                                                                Text("Normal"),
                                                                Text("Mal"),
                                                                Text("Muy Mal"),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    actions: [
                                                      Container(
                                                        height: 60,
                                                        width:
                                                            MediaQuery.of(context)
                                                                    .size
                                                                    .width +
                                                                10,
                                                        alignment:
                                                            Alignment.center,
                                                        child: Resusable_Button(
                                                          textSize: 15,
                                                          text: "ok",
                                                          onPress: () {
                                                            setState(() {
                                                              showProgress=true;
                                                            });

                                                            Navigator.of(
                                                                context)
                                                                .pop();
                                                            ProgressionController
                                                                .instance
                                                                .updateState(
                                                                    state)
                                                                .then((value) {
                                                              DataBaseProvider
                                                                  .instance
                                                                  .deleteExer()
                                                                  .then((value) {

                                                              });
                                                            });
                                                          },
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                }).then((value) {
                                              setState(() {
                                                showProgress=false;
                                              });
                                              Navigator.pop(context);
                                              Navigator.popAndPushNamed(context, SequencesScreen.id);
                                            });
                                          },
                                          child: Center(
                                            child: Text(
                                              "Si",
                                              style: kMediumTextStyle,
                                            ),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                            primary: DarkPurple,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              elevation: 10,
                              backgroundColor: KbackGroundColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)),
                            );
                          });
                    },
                    style: ButtonStyle(
                      overlayColor: MaterialStateProperty.all<Color>(kDarkPink),
                    ),
                  ),
                ),
                centerTitle: true,
                actions: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
                    child: TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, ProfileCard.id);
                      },
                      child: Hero(
                        tag: "ProfilePic",
                        child: CircleAvatar(
                          radius: 20,
                          backgroundColor: KbackGroundColor,
                          backgroundImage:
                              LoginController.currentUser.ProfilepickUrl == ""
                                  ? AssetImage("images/defaultprofile.jpg")
                                  : CachedNetworkImageProvider(
                                      LoginController.currentUser.ProfilepickUrl,
                                    ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: SafeArea(
                child: Container(
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: ProgressionController.exer.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 80,
                          width: MediaQuery.of(context).size.width - 50,
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: ReusableCard(
                              onPress: ProgressionController.exer[index].isDone ==
                                      false
                                  ? () {
                                      ExercisePopUp pop = ExercisePopUp();
                                      pop.showDialogPopUp(
                                          ProgressionController.exer[index].name,
                                          ProgressionController.exer[index].time
                                              .toString(),
                                          1,
                                          index,
                                          context);
                                    }
                                  : () {},
                              backColor: Color(0xFF675F73),
                              children:
                                  ProgressionController.exer[index].isDone == true
                                      ? Text("Completo",
                                          style: kMediumTextStyle.copyWith(
                                              fontSize: 20.0))
                                      : Text(
                                          "Secuencia " + (index + 1).toString(),
                                          style: kMediumTextStyle.copyWith(
                                              fontSize: 20.0),
                                        ),
                            ),
                          ),
                        );
                      }),
                ),
              ),
            ),
          );
        } else {
          return LodingWidget();
        }
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Components/Reusable_Card.dart';
import 'package:minihomelab/screens/Home_Page.dart';
import 'package:minihomelab/screens/Level_screen.dart';
import 'package:minihomelab/screens/Progress_Screen.dart';
import 'package:minihomelab/screens/statistics_screen.dart';

class BottomMenuIcons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          Expanded(
            child: ReusableCard(
              marginAll: 5.0,
              backColor: Color(0xFF675F73),
              height: 65.0,
              children: Icon(
                Icons.bar_chart_outlined,
                color: KbackGroundColor,
                size: 50.0,
              ),
              onPress: () {
                Navigator.popAndPushNamed(context, ProgressScreen.id);
              },
            ),
          ),
          Expanded(
            child: ReusableCard(
              marginAll: 5.0,
              backColor: Color(0xFF675F73),
              height: 65.0,
              children: Icon(
                Icons.date_range_outlined,
                color: KbackGroundColor,
                size: 50.0,
              ),
            ),
          ),
          Expanded(
            child: ReusableCard(
              onPress: () {
                Navigator.pushNamed(context, HomePage.id);
              },
              marginAll: 5.0,
              backColor: Color(0xFF675F73),
              height: 65.0,
              children: Icon(
                Icons.home_outlined,
                color: KbackGroundColor,
                size: 50.0,
              ),
            ),
          ),
          Expanded(
            child: ReusableCard(
              marginAll: 5.0,
              backColor: Color(0xFF675F73),
              height: 65.0,
              children: Icon(
                Icons.fitness_center_outlined,
                color: KbackGroundColor,
                size: 50.0,
              ),
              onPress: () {
                Navigator.pushNamed(context, LevelScreen.id);
              },
            ),
          ),
          Expanded(
            child: ReusableCard(
              marginAll: 5.0,
              backColor: Color(0xFF675F73),
              height: 65.0,
              children: Icon(
                Icons.accessibility,
                color: KbackGroundColor,
                size: 50.0,
              ),
              onPress: () {
                Navigator.pushNamed(context, StatisticsScreen.id);
              },
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Components/BottomMenuIcons.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:minihomelab/Controllers/Statistics_Controller.dart';
import 'package:minihomelab/models/Statistics_Model.dart';

class StatisticsScreen extends StatelessWidget {
  static const String id = 'statistics_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          FutureBuilder(
            future: StatisticsController.instance
                .GetAllStatistics(LoginController.currentUser.userId),
            builder:
                (BuildContext context, AsyncSnapshot<Statistics> snapshot) {
              if (snapshot.hasData) {
                FirebaseAnalytics().logEvent(
                    name: 'view_statistics_event', parameters: null);

                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text("Estadísticas del Usuario",
                        style: TextStyle(color: kligtgrey, fontSize: 40)),
                    Column(children: [
                      Text("Ejercicios\ncompletados",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: kligtgrey, fontSize: 30)),
                      Text(snapshot.data.exercise.numCompleted.toString(),
                          style: TextStyle(
                            color: kligtgrey,
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                          )),
                    ]),
                    Column(children: [
                      Text("Ejercicios\nno completados",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: kligtgrey, fontSize: 30)),
                      Text(snapshot.data.exercise.numNotCompleted.toString(),
                          style: TextStyle(
                            color: kligtgrey,
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                          )),
                    ]),
                    Column(children: [
                      Text("Última vez que realizó\nun ejercicio",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: kligtgrey, fontSize: 30)),
                      Text(
                        "Hace " +
                            snapshot.data.usage.lastDaysCount.toString() +
                            " días",
                        style: TextStyle(
                          color: kligtgrey,
                          fontWeight: FontWeight.bold,
                          fontSize: 40,
                        ),
                      ),
                    ]),
                  ],
                );
              } else {
                return Column(
                  children: [
                    Container(
                      color: Color.fromARGB(0, 0, 0, 0),
                      padding: EdgeInsets.fromLTRB(0.0, 62.0, 0.0, 250.0),
                      child: Text("Estadísticas del Usuario",
                          style: TextStyle(color: kligtgrey, fontSize: 40)),
                    ),
                    Center(child: CircularProgressIndicator()),
                  ],
                );
              }
            },
          ),
          Container(
            color: Color.fromARGB(0, 0, 0, 0),
            padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 0),
            child: BottomMenuIcons(),
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
      ),
    );
  }
}

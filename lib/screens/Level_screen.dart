import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/Components/Reusable_Card.dart';
import 'package:minihomelab/Components/BottomMenuIcons.dart';
import 'package:minihomelab/screens/sequences_screen.dart';

class LevelScreen extends StatelessWidget {
  static const String id = 'level_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: BoxDecoration(color: KbackGroundColor),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Hero(
                tag: "LevelTitle",
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    "Niveles",
                    style: kTitleText,
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Center(
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: ReusableCard(
                              onPress: () {
                                Navigator.pushNamed(context, SequencesScreen.id, arguments: SequencesArguments(level: 1));
                              },
                              backColor: Color(0xFF675F73),
                              children: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: Icon(
                                      Icons.hourglass_bottom_outlined,
                                      color: KbackGroundColor,
                                    ),
                                  ),
                                  Text(
                                    "1",
                                    style:
                                        kMediumTextStyle.copyWith(fontSize: 90.0),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Icon(
                                  Icons.lock_outline,
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                "2",
                                style:
                                    kMediumTextStyle.copyWith(fontSize: 90.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Icon(
                                  Icons.lock_outline,
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                "3",
                                style:
                                    kMediumTextStyle.copyWith(fontSize: 90.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Icon(
                                  Icons.lock_outline,
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                "4",
                                style:
                                    kMediumTextStyle.copyWith(fontSize: 90.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Icon(
                                  Icons.lock_outline,
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                "5",
                                style:
                                    kMediumTextStyle.copyWith(fontSize: 90.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: ReusableCard(
                          backColor: Color(0xFF675F73),
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.topRight,
                                child: Icon(
                                  Icons.lock_outline,
                                  color: KbackGroundColor,
                                ),
                              ),
                              Text(
                                "6",
                                style:
                                    kMediumTextStyle.copyWith(fontSize: 90.0),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              BottomMenuIcons(),
            ],
          ),
        ),
      ),
    );
  }
}

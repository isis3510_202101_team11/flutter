import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../models/Parsers/Json_parser.dart';
import '../../Providers/HTTP_Client.dart';

/// * Contains the minimal CRUD logic that must be implemented by a provider.
/// It can also be used to create "mock" classes for easy unit testing.
class CrudRepository<T> {
  const CrudRepository({
    @required this.jsonParser,
  });

  final JsonParser<T> jsonParser;

  /// * Get - retrieve objects
  /// T - data serializer type
  /// api - API endpoint
  /// parser - JsonParser serializer type T
  Future<T> getAll(String api) {
    return RequestREST(api: api).executeGet<T>(jsonParser);
  }

  /// * Get - retrieve one object
  /// T - data serializer type
  /// api - API endpoint
  /// parser - JsonParser serializer type T
  Future<T> getOne(String api) {
    return RequestREST(api: api).executeGetOne<T>(jsonParser);
  }

  /// * Put - updates an object
  /// T - data serializer type
  /// api - API endpoint
  /// parser - JsonParser serializer type T
  Future<T> put( String api, Map<String, dynamic> data, FormData formData) {
    return RequestREST(api: api, data: data)
        .executePut<T>(jsonParser, formData);
  }

  /// * Post - creates an object
  /// T - data serializer type
  /// api - API endpoint
  /// parser - JsonParser serializer type T
  Future<T> post(String api, Map<String, dynamic> data) {
    return RequestREST(api: api, data: data).executePost<T>(jsonParser);
  }

  /// * Delete - removes an object
  /// T - data serializer type
  /// api - API endpoint
  /// parser - JsonParser serializer type T
  Future<T> delete(String api) {
    return RequestREST(api: api).executeDelete<T>(jsonParser);
  }
}

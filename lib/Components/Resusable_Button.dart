import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';

class Resusable_Button extends StatelessWidget {
  String text;
  double textSize;
  Function onPress;
  bool enableB;
  Resusable_Button({this.text, this.onPress, this.textSize = 30,this.enableB =true});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 100.0),
      height: 50.0,
      child: ElevatedButton(

        onPressed: enableB ? onPress:null,

        child: Center(
          child: Text(
            text,
            style: kMediumTextStyle.copyWith(fontSize: textSize),
          ),
        ),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(kDarkPink),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
              side: BorderSide(color: Color(0xFF3E4259)),
            ),
          ),
        ),

      ),
    );
  }
}

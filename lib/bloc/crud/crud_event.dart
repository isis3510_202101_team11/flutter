part of 'crud_bloc.dart';

/// * Evento trigger del bloc
abstract class CrudEvent {
  const CrudEvent();

  @override
  List<Object> get props => [];
}

/// * Evento petición de un recurso HTTP para obtener objetos tipo T
/// @post : se descargan los objetos a la aplicación
/// * Activa cualquier peticición de request
class CrudRequestedEvent<T> extends CrudEvent {
  const CrudRequestedEvent({
    @required this.api,
    @required this.dataParser,
    //Enumeracion para saber el tipo de solicitud que estoy haciendo
    @required this.requestType,
    this.body,
    this.fromData,
  });

  /// * API Endpoint
  final String api;

  /// * JSON Serializer parser to Object
  final JsonParser<T> dataParser;

  /// * Tipo de petición
  final RequestType requestType;

  /// * Cuerpo de la petición en caso de tratarse de un PUT o POST
  final Map<String, dynamic> body; // request body opcional

  /// * Form data
  final FormData fromData;

}

class CrudSetUserId extends CrudEvent{
  const CrudSetUserId({
    this.userId,});
    final String userId;
}
/// * Tipos de peticiones HTTP para especificar el tipo de request en el CrudRequestedEvent event
enum RequestType {
  getRequest,
  getOneRequest,
  putRequest,
  postRequest,
  deleteRequest,
}

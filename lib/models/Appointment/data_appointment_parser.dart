import '../Parsers/Json_parser.dart';
import 'data_appointment_model.dart';

class DataAppointmentParser extends JsonParser<DataAppointmentModel>
    with ObjectDecoder<DataAppointmentModel> {
  const DataAppointmentParser();

  @override
  Future<DataAppointmentModel> parseFromJson(String json) async {
    final decode = decodeJsonObject(json);
    return DataAppointmentModel.fromJson(decode);
  }
}
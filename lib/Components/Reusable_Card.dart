import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';

class ReusableCard extends StatelessWidget {
  final Color backColor;
  final Widget children;
  final Function onPress;
  final double marginAll;
  final double height;
  ReusableCard(
      {this.backColor = Colors.white,
      this.children,
      this.onPress,
      this.marginAll = 8.0,
      this.height = double.infinity});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: TextButton(
        child: children,
        onPressed: onPress,
        style: ButtonStyle(
          overlayColor: MaterialStateProperty.all<Color>(kDarkPink),
        ),
      ),
      margin: EdgeInsets.all(marginAll),
      decoration: BoxDecoration(
          color: backColor,
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: [
            BoxShadow(
              color: kPrimaryDarkGrey,
              offset: Offset(2.5, 2.5),
              blurRadius: 1.5,
            ),
          ]),
    );
  }
}

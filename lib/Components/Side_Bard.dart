import 'package:flutter/material.dart';
import 'package:minihomelab/Constanse.dart';

class SideBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: KbackGroundColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.all(1),
                    child: Icon(
                      Icons.medical_services,
                      size: 50,
                      color: Color(0xFF3E4259),
                    ),
                  ),
                  Text(
                    "Mini-Home Lab",
                    style: kTitleText.copyWith(fontSize: 30),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            TextButton(
              onPressed: () {},
              child: Text(
                "Sobre nosotros",
                style: kMediumTextStyle.copyWith(color: kligtgrey),
              ),
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all<Color>(kDarkPink),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: Text(
                "Preguntas Frecuentes",
                style: kMediumTextStyle.copyWith(color: kligtgrey),
              ),
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all<Color>(kDarkPink),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: Text(
                "Soporte",
                style: kMediumTextStyle.copyWith(color: kligtgrey),
              ),
              style: ButtonStyle(
                overlayColor: MaterialStateProperty.all<Color>(kDarkPink),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:async';
import 'package:flutter/material.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/components/explanation_popup.dart';
import 'package:minihomelab/components/feedback_popup.dart';
import 'package:minihomelab/Controllers/Sequence_Controller.dart';
import 'package:vibration/vibration.dart';
import 'package:sensors/sensors.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import '../Constanse.dart';
import '../Controllers/DataBase_Provider.dart';
import '../Controllers/Progression_Controller.dart';
import '../Controllers/Sequence_Controller.dart';

class SequenceTimer extends StatefulWidget {
  SequenceTimer({Key key, this.seconds, this.number, this.level})
      : super(key: key);
  final int seconds;
  final int number;
  final int level;

  @override
  _TimerState createState() =>
      _TimerState(seconds: seconds, number: number, level: level);
}

class Measure {
  final double x;
  final double y;
  final double z;
  final int t;

  Measure(
      {@required this.x, @required this.y, @required this.z, @required this.t});

  Map<String, dynamic> toMap() {
    return {
      "x": x,
      "y": y,
      "z": z,
      "t": t,
    };
  }
}

class _TimerState extends State<SequenceTimer> {
  _TimerState(
      {@required this.seconds, @required this.number, @required this.level});

  final int seconds;
  final int number;
  final int level;

  Timer _timer;
  int _dateStarted;
  int _current = 0;
  bool started = false;
  bool finished = false;
  bool underTreshold = true;

  StreamSubscription<dynamic> _streamSubscription;
  UserAccelerometerEvent _measuredAcceleration;
  List<Measure> _measures = [];

  void start() {
    FirebaseAnalytics().logEvent(name: 'start_session_event', parameters: null);

    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_measuredAcceleration != null) {
          _measures.add(Measure(
            x: _measuredAcceleration.x,
            y: _measuredAcceleration.y,
            z: _measuredAcceleration.z,
            t: _current,
          ));
        }

        if (_measuredAcceleration != null &&
            (_measuredAcceleration.x.abs() >
                    ProgressionController.exer[number - 1].ResX ||
                _measuredAcceleration.y.abs() >
                    ProgressionController.exer[number - 1].ResY ||
                _measuredAcceleration.z.abs() >
                    ProgressionController.exer[number - 1].ResZ)) {
          underTreshold = false;
          Vibration.vibrate(duration: 500);
        } else {
          underTreshold = true;
        }

        if (_current == seconds) {
          setState(() {
            finish();
          });
        } else {
          setState(() {
            _current++;
          });
        }
      },
    );

    _dateStarted = DateTime.now().millisecondsSinceEpoch;

    _streamSubscription = userAccelerometerEvents.listen(
        (UserAccelerometerEvent event) => _measuredAcceleration = event);
    started = true;
  }

  void finish() async {
    _timer.cancel();
    _streamSubscription.cancel();

    setState(() {
      started = false;
      finished = true;
    });

    FirebaseAnalytics()
        .logEvent(name: 'finish_session_event', parameters: null);

    showDialog(
      context: context,
      builder: (BuildContext context) => _buildFeedbackPopup(context),
    );

    addSession();

    ProgressionController.exer[number - 1].isDone = true;
    ProgressionController.currentExercise.isDone = true;
    await DataBaseProvider.instance
        .updateExer(ProgressionController.exer[number - 1]);
  }

  void abandon() {
    if (_timer != null) {
      _timer.cancel();
    }

    if (_streamSubscription != null) {
      _streamSubscription.cancel();
    }

    FirebaseAnalytics()
        .logEvent(name: 'abandon_session_event', parameters: null);

    Navigator.of(context).pop();
  }

  Future<void> addFeedback(int feedback) {
    return SequenceController.instance
        .rateExercise(ProgressionController.exer[number - 1].ExerId, feedback);
  }

  Future<void> addSession() {
    if (_measures.length == 0) {
      return null;
    }

    final String userId = LoginController.currentUser.userId;
    Map session = {
      "level": level,
      "number": number,
      "seconds": _current,
      "completed": _current == seconds,
      "date": _dateStarted,
      "measures": [for (var m in _measures) m.toMap()],
    };

    _measures = [];

    return SequenceController.instance
        .sendSession(userId, session)
        .then((docs) => null)
        .catchError((error) {});
  }

  void restart() {
    _timer.cancel();
    _streamSubscription.cancel();
    _dateStarted = null;
    _measures = [];

    setState(() {
      started = false;
      _current = 0;
    });
  }

  void showExplanation() {
    showDialog(
      context: context,
      builder: (BuildContext context) => _buildExplanationPopup(context),
    );
  }

  Widget _buildFeedbackPopup(BuildContext context) {
    return FeedbackPopup(
      completed: _current == seconds,
      onSubmit: addFeedback,
    );
  }

  Widget _buildExplanationPopup(BuildContext context) {
    return ExplanationPopup(
      explanation: ProgressionController.exer[number - 1].Explicacion,
      image: ProgressionController.exer[number - 1].URL,
    );
  }

  String getIndication() {
    if (!started) {
      return "";
    }
    if (underTreshold) {
      return "¡Sigue los indicadores LED!";
    }

    return "¡Estás superando el límite permitido, ten cuidado!";
  }

  TextStyle getIndicationStyle() {
    if (underTreshold) {
      return TextStyle(color: kligtgrey, fontSize: 30);
    }

    return TextStyle(color: kDarkPink, fontSize: 30);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Tiempo restante",
                style: TextStyle(fontSize: 30, color: DarkPurple),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                decoration: BoxDecoration(
                    border: Border.all(color: LightPink, width: 3),
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: Text(
                    "${(seconds - _current) ~/ 60}".padLeft(2, "0") +
                        ":" +
                        "${(seconds - _current) % 60}".padLeft(2, "0"),
                    style: TextStyle(color: kligtgrey, fontSize: 40)),
              ),
            ],
          ),
          Text(
            getIndication(),
            style: getIndicationStyle(),
            textAlign: TextAlign.center,
          ),
          Row(
            children: [
              if (!started &&
                  !finished &&
                  ProgressionController.exer[number - 1].Explicacion != "")
                OutlinedButton(
                    onPressed: showExplanation,
                    child: Text(
                      "Explicación",
                      style: TextStyle(color: kDarkPink),
                    )),
              if (!started && !finished)
                OutlinedButton(
                    onPressed: start,
                    child: Text(
                      "Iniciar secuencia",
                      style: TextStyle(color: kDarkPink),
                    )),
              if (finished)
                OutlinedButton(
                    onPressed: abandon,
                    child:
                        Text("Abandonar", style: TextStyle(color: kDarkPink))),
              if (started && !finished)
                OutlinedButton(
                    onPressed: finish,
                    child: Text("Abandonar secuencia",
                        style: TextStyle(color: kDarkPink))),
              if (started && !finished)
                OutlinedButton(
                    onPressed: restart,
                    child: Text("Reiniciar secuencia",
                        style: TextStyle(color: kDarkPink)))
            ],
            mainAxisAlignment: MainAxisAlignment.spaceAround,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
    );
  }
}

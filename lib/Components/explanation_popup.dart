import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../Constanse.dart';

class ExplanationPopup extends StatelessWidget {
  final String explanation;
  final String image;

  ExplanationPopup({this.explanation, this.image});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
      elevation: 16,
      child: Container(
        height: 500,
        width: 360,
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              "Explicación",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 30,
                color: kDarkPink,
                fontWeight: FontWeight.bold,
              ),
            ),
            if (image != "") Image(image: CachedNetworkImageProvider(image)),
            Text(
              explanation,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                color: kPrimaryDarkGrey,
              ),
            ),
            OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (Set<MaterialState> states) {
                      return DarkPurple; // Use the component's default.
                    },
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("ACEPTAR",
                    style: TextStyle(
                        color: Color.fromARGB(255, 255, 255, 255)))),
          ],
        ),
      ),
    );
  }
}

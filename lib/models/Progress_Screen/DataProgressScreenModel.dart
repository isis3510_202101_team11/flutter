import 'package:json_annotation/json_annotation.dart';

//Fuerza la conversión de Json a objeto
part 'DataProgressScreenModel.g.dart';

@JsonSerializable(explicitToJson: true)
class DataProgressScreenModel {
  const DataProgressScreenModel({
    this.dataRomMonth,
    this.dataTiempoRWeek,
    this.dataFuerzaMonth,
    this.dataFuerzaWeek,
    this.dataRomWeek,
    this.dataTiempoRMonth,
    this.destacadosFuerzaMonth,
    this.destacadosFuerzaWeek,
    this.destacadosRomMonth,
    this.destacadosRomWeek,
    this.destacadosTiempoRMonth,
    this.destacadosTiempoRWeek,
  });

  /// Construyo un objeto a partir del Json
  factory DataProgressScreenModel.fromJson(Map<String, dynamic> json) =>
      _$DataProgressScreenModelFromJson(json);

  /// Lista de atributos, son genericos para los tres tipos de datos que voy a mostrar
  final List<Map<String, double>> dataRomWeek;
  final List<Map<String, double>> dataRomMonth;
  final List<Map<String, String>> destacadosRomWeek;
  final List<Map<String, String>> destacadosRomMonth;

  final List<Map<String, double>> dataFuerzaWeek;
  final List<Map<String, double>> dataFuerzaMonth;
  final List<Map<String, String>> destacadosFuerzaWeek;
  final List<Map<String, String>> destacadosFuerzaMonth;

  final List<Map<String, double>> dataTiempoRWeek;
  final List<Map<String, double>> dataTiempoRMonth;
  final List<Map<String, String>> destacadosTiempoRWeek;
  final List<Map<String, String>> destacadosTiempoRMonth;

  /// A partir del objeto retorno el Json
  Map<String, dynamic> toJson() => _$DataProgressScreenModelToJson(this);
}

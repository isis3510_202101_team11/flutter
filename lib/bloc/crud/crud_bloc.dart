import 'dart:async';
import 'dart:convert';
//Para saber si hay conexión a internet
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

import '../../models/Progress_Screen/DataProgressScreenModel.dart';
import '../../models/Parsers/Json_parser.dart';
import '../repositories/crud_repository.dart';

part 'crud_event.dart';
part 'crud_state.dart';

class CrudBloc<T> extends Bloc<CrudEvent, CrudState> with HydratedMixin {
  //No es constante porque el repo puede cambiar e inicializo el crud inicial
  //El repo es el que definimos desde los Screens
  CrudBloc({@required this.repo}) : super(CrudInitial());

  //Es la conexion al back pero asociandole un modelo
  final CrudRepository<T> repo;
  String userId;

  //Mapeo un evento que me llega del front a un estado que mando
  @override
  Stream<CrudState> mapEventToState(
    CrudEvent event,
    //Es un canal que siempre esta prendido, veo que estados estan navegando en el rio. es un string, entonces me suscribo a ese canal
  ) async* {
    final connectivityResult = await Connectivity().checkConnectivity();
    //if (event is CrudSetUserId){
    //  this.userId = event.userId;
     // yield CrudLoadSuccess(dataObject: null);
     // return;
    //}
    
    if (connectivityResult == ConnectivityResult.none) {
      //Si no hay conexion paso a este estado
      yield CrudLoadNoConnection();
      //Pero tengo que parar la conexion
      return;
    }

    if (event is CrudRequestedEvent<T>) {
      // * Load data - HTTP Request
      // Por el thread le mando el estado de que estoy cargando
      yield CrudLoadInProgress();
      //Acá si estoy solucionando el evento que me está llegando por el río
      yield* _onRequest(event: event);
    }
  }

  /// * Procesamiento de un simple request al back
  Stream<CrudState> _onRequest({CrudRequestedEvent<T> event}) async* {
    try {
      final requestType = event.requestType;
      final body = event.body; // Map<String, dynamic>

      //No me está mandando ningun request
      if (requestType == null) {
        yield CrudInvalidRequestType();
      } else {
        //Es lo que vamos a retornar, lo que muestra el front
        T dataObject;
        //Sobre los posibles casos que defini en la lista
        switch (requestType) {
          case RequestType.getRequest:
            //El await hace que se siga con el codigo hasta que halla respuesta, acá es el multithreading, 
            //frena la ejecución de este pedazo de código pero todo el resto sigue
            dataObject = await repo.getAll(event.api);
            yield CrudLoadSuccess<T>(dataObject: dataObject);
            break;
          case RequestType.getOneRequest:
            dataObject = await repo.getOne(event.api);
            yield CrudLoadSuccess<T>(dataObject: dataObject);
            break;
          case RequestType.deleteRequest:
            dataObject = await repo.delete(event.api);
            yield CrudLoadSuccess<T>(dataObject: dataObject);
            break;
          case RequestType.putRequest:
            dataObject = await repo.put(event.api, body, event.fromData);
            yield CrudLoadSuccess<T>(dataObject: dataObject);
            break;
          case RequestType.postRequest:
            dataObject = await repo.post(event.api, body);
            yield CrudLoadSuccess<T>(dataObject: dataObject);
            break;
          default:
            dataObject = null;
            yield CrudLoadFailure();
            break;
        }
      }
    } catch (error) {

      final encoded =
          'API :: ${jsonEncode(event.api)} -- REQ :: ${event.requestType}';
          print(encoded);
          print(error);
      yield CrudLoadFailure();
    }
  }

  /// * ---------------------------------------------------------
  /// AHORA SI GUARDO EN EL CELULAR B)

  //Toma desde el celular
  //Cojo el archivo que tengo en la ubicación especial, me importan los archivos de serialización
  //Se activa sola si entro en el estado inicial del bloc
  @override
  CrudState fromJson(Map<String, dynamic> json) {
    if (T is DataProgressScreenModel) {
      return CrudLoadSuccess<DataProgressScreenModel>(
        dataObject: DataProgressScreenModel.fromJson(json),
      );
    }
    ;
    return CrudInitial();
  }

  //Guardo al celular
  //Siempre que hago yield se activa el metodo
  @override
  Map<String, dynamic> toJson(CrudState state) {
    //Si obtengo algo del back si serializo
    if (state is CrudLoadSuccess<T>) {
      if (T is DataProgressScreenModel) {
        final data = state.dataObject as DataProgressScreenModel;
        return data?.toJson();
      }
    }
    return <String, dynamic>{};
  }
}

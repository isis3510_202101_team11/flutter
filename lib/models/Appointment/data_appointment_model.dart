import 'package:json_annotation/json_annotation.dart';

part 'data_appointment_model.g.dart';

@JsonSerializable(explicitToJson: true)
class DataAppointmentModel {
  const DataAppointmentModel({
    this.appointDate,
    this.createdDate,
    this.doctorName,
    this.modifyDate,
    this.state,
    this.type,
    this.id,
  });

  factory DataAppointmentModel.fromJson(Map<String, dynamic> json) =>
      _$DataAppointmentModelFromJson(json);

  @JsonKey(name: 'ApointDate')
  final String appointDate;
  @JsonKey(name: 'CreatDate')
  final String createdDate;
  @JsonKey(name: 'DoctorName')
  final String doctorName;
  @JsonKey(name: 'Estate')
  final int state;
  @JsonKey(name: 'ModifyDate')
  final String modifyDate;
  @JsonKey(name: 'Type')
  final String type;
  @JsonKey(name: 'appointmentId')
  final String id;

  Map<String, dynamic> toJson() => _$DataAppointmentModelToJson(this);
}
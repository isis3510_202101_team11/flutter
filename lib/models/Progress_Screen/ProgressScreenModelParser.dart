import '../Parsers/Json_parser.dart';
import 'DataProgressScreenModel.dart';

class ProgressScreenModelParser extends JsonParser<DataProgressScreenModel>
    with ObjectDecoder<DataProgressScreenModel> {
  const ProgressScreenModelParser();

  //Sobreescribo el método que parse desde Json
  @override
  Future<DataProgressScreenModel> parseFromJson(String json) async {
    //Viene de object decoder
    final decode = decodeJsonObject(json);
    //Clase le entra un atributo de tipo Map string dynamic
    //Metodo constructor a partir del Json
    return DataProgressScreenModel.fromJson(decode);
  }
}

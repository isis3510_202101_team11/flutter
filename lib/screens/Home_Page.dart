import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:minihomelab/Components/Resusable_Button.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';
import 'package:minihomelab/Controllers/Progression_Controller.dart';
import 'package:minihomelab/components/Reusable_Card.dart';
import 'package:minihomelab/Constanse.dart';
import 'package:minihomelab/screens/Progress_Screen.dart';
import 'package:minihomelab/screens/appointment_screen.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:minihomelab/components/verticalProgressbar.dart';
import 'package:minihomelab/components/Side_Bard.dart';
import 'package:minihomelab/screens/Level_screen.dart';
import 'package:minihomelab/Components/BottomMenuIcons.dart';
import 'package:minihomelab/screens/Profile_Screen.dart';
import 'package:minihomelab/Controllers/Login_Controller.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  static const String id = 'home_screen';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  var valuer;
  int state = 0;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      duration: Duration(seconds: 1),
      vsync: this,
    );
    controller.forward();

    SharedPreferences.getInstance().then((pref) {
      DateTime _dateTime = DateTime.parse(pref.getString('LastLoginDate'));

      if (_dateTime != null) {
        print(_dateTime.toString() + "segundo asdas");
        if (_dateTime.day != DateTime.now().day) {
          pref.setString("LastLoginDate", DateTime.now().toString());
          DataBaseProvider.instance.deleteAllExerExer();
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text(
                      "¿Como te sientes el día de hoy?",
                      style: kTitleText.copyWith(fontSize: 25),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    content: Container(
                      height: 200,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Expanded(
                            child: CupertinoPicker(
                              itemExtent: 30,
                              useMagnifier: true,
                              backgroundColor: KbackGroundColor,
                              onSelectedItemChanged: (inte) {
                                state = 5 - inte;
                              },
                              children: [
                                Text("Muy bien"),
                                Text("bien"),
                                Text("Normal"),
                                Text("Mal"),
                                Text("Muy Mal"),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    actions: [
                      Container(
                        height: 60,
                        width: MediaQuery.of(context).size.width + 20,
                        alignment: Alignment.center,
                        child: Resusable_Button(
                          textSize: 15,
                          text: "Ok",
                          onPress: () {
                            ProgressionController.instance.updateState(state);
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  );
                });
          });
        }
      }
    });

    controller.addListener(() {
      setState(() {
        valuer = controller.value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mini-Home Lab"),
        centerTitle: true,
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
            child: TextButton(
              onPressed: () {
                Navigator.pushNamed(context, ProfileCard.id);
              },
              child: Hero(
                tag: "ProfilePic",
                child: CircleAvatar(
                  radius: 20,
                  backgroundColor: KbackGroundColor,
                  backgroundImage:
                      LoginController.currentUser.ProfilepickUrl == ""
                          ? AssetImage("images/defaultprofile.jpg")
                          : CachedNetworkImageProvider(
                              LoginController.currentUser.ProfilepickUrl,
                            ),
                ),
              ),
            ),
          ),
        ],
      ),
      drawer: Drawer(
        child: SideBarWidget(),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: ReusableCard(
              backColor: Color(0xFF675F73),
              children: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.all(2.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          width: 120.0,
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: Icon(
                            Icons.notifications_none_outlined,
                            color: KbackGroundColor,
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: KbackGroundColor,
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Container(
                      height: 110.0,
                      child: ListView(
                        padding: EdgeInsets.all(0.0),
                        shrinkWrap: true,
                        children: [
                          ListRow(
                            worktext: "Rutina Diaria",
                            secondtext: "30 min",
                          ),
                          ListRow(
                            worktext: "Medicamentos",
                            secondtext: "15:30",
                          ),
                          ListRow(
                            worktext: "Terapia fría",
                            secondtext: "45 min",
                          ),
                          ListRow(
                            worktext: "Terapia fría",
                            secondtext: "45 min",
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: ReusableCard(
              backColor: Color(0xFF675F73),
              onPress: () {
                Navigator.pushNamed(context, ProgressScreen.id);
              },
              children: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    "Progreso",
                    style: kMediumTextStyle,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      VerticalProgressByDay(
                        dayLeter: "L",
                        porcentageFill: 0.5 * valuer,
                      ),
                      VerticalProgressByDay(
                        dayLeter: "M",
                        porcentageFill: 0.4 * valuer,
                      ),
                      VerticalProgressByDay(
                        dayLeter: "I",
                        porcentageFill: 0.8 * valuer,
                      ),
                      VerticalProgressByDay(
                        dayLeter: "J",
                        porcentageFill: 0.1 * valuer,
                      ),
                      VerticalProgressByDay(
                        dayLeter: "V",
                        porcentageFill: 0.8 * valuer,
                      ),
                      VerticalProgressByDay(
                        dayLeter: "S",
                        porcentageFill: 0.9 * valuer,
                      ),
                      VerticalProgressByDay(
                        dayLeter: "D",
                        porcentageFill: 0.2 * valuer,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Row(
              children: [
                Expanded(
                  child: ReusableCard(
                    backColor: Color(0xFF675F73),
                    onPress: () =>
                        Navigator.pushNamed(context, AppointmentScreen.id),
                    children: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          "Citas Médicas",
                          style: TextStyle(
                            fontSize: 30.0,
                            color: KbackGroundColor,
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: FutureBuilder(
                    future: ProgressionController.instance.GetCompleteExercises(
                        LoginController.currentUser.userId),
                    builder:
                        (BuildContext context, AsyncSnapshot<double> snapshot) {
                      if (snapshot.hasData) {
                        return ReusableCard(
                          onPress: () {
                            Navigator.popAndPushNamed(context, LevelScreen.id);
                          },
                          backColor: Color(0xFF675F73),
                          children: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Nivel 1",
                                style: TextStyle(
                                  fontSize: 18.0,
                                  color: KbackGroundColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: CircularPercentIndicator(
                                  radius: 100.0,
                                  lineWidth: 15.0,
                                  percent: snapshot.data * valuer,
                                  center: Text(
                                    (snapshot.data * 100.0).toInt().toString(),
                                    style: TextStyle(
                                      fontSize: 20.0,
                                      color: KbackGroundColor,
                                    ),
                                  ),
                                  progressColor: kDarkPink,
                                  backgroundColor: KbackGroundColor,
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Center(child: CircularProgressIndicator());
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
          BottomMenuIcons(),
        ],
      ),
    );
  }
}

class ListRow extends StatelessWidget {
  final String worktext;
  final String secondtext;

  ListRow({this.worktext = "", this.secondtext = ""});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 25,
            width: 25,
            child: Checkbox(
              value: false,
            ),
          ),
          Text(
            worktext,
            style: kMediumTextStyle.copyWith(color: kligtgrey, fontSize: 13.0),
          ),
          SizedBox(
            width: 150,
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              secondtext,
              style:
                  kMediumTextStyle.copyWith(color: kligtgrey, fontSize: 15.0),
            ),
          ),
        ],
      ),
    );
  }
}

class ProfilePhoto extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

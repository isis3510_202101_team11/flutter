import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:minihomelab/bloc/crud/crud_bloc.dart';
import 'package:minihomelab/bloc/observers/bloc_observer.dart';
import 'package:minihomelab/bloc/repositories/crud_repository.dart';
import 'package:minihomelab/models/Appointment/data_appointment_model.dart';
import 'package:minihomelab/models/Appointment/data_appointment_parser.dart';
import 'package:minihomelab/models/Progress_Screen/DataProgressScreenModel.dart';
import 'package:minihomelab/models/Progress_Screen/ProgressScreenModelParser.dart';
import 'package:minihomelab/screens/Progress_Screen.dart';
import 'package:minihomelab/screens/statistics_screen.dart';
import 'package:minihomelab/screens/appointment_screen.dart';
import 'screens/Home_Page.dart';
import 'Constanse.dart';
import 'screens/sequence_screen.dart';
import 'screens/Welcome_page.dart';
import 'screens/SingUp_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:minihomelab/screens/Level_screen.dart';
import 'package:minihomelab/screens/sequences_screen.dart';
import 'package:path_provider/path_provider.dart';
import 'package:minihomelab/screens/Profile_Screen.dart';
import 'screens/Progress_Screen.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /// Hydrated storage
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );

  /// * Bloc observer
  Bloc.observer = CrudBlocObserver();

  const progresoRepository = CrudRepository<DataProgressScreenModel>(
    jsonParser: ProgressScreenModelParser(),
  );

  const appointmentsRepo = CrudRepository<DataAppointmentModel>(
    jsonParser: DataAppointmentParser(),
  );

  await Firebase.initializeApp();
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
  runApp(MiniHomeLab(
    progressScreenRepo: progresoRepository,
    appointmentsRepo: appointmentsRepo,
  ));
}

class MiniHomeLab extends StatelessWidget {
  final CrudRepository<DataProgressScreenModel> progressScreenRepo;

  //Inicializo la variable progress Screen Repo
  //Key renderizo y diferencio los widgets
  const MiniHomeLab(
      {@required this.progressScreenRepo,
      @required this.appointmentsRepo,
      Key key})
      : super(key: key);

  final CrudRepository<DataAppointmentModel> appointmentsRepo;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) =>
              CrudBloc<DataProgressScreenModel>(repo: progressScreenRepo),
        ),
        BlocProvider(
          create: (context) =>
              CrudBloc<DataAppointmentModel>(repo: appointmentsRepo),
        )
      ],
      child: MaterialApp(
        theme: ThemeData.light().copyWith(
          scaffoldBackgroundColor: KbackGroundColor,
          primaryColor: DarkPurple,
          primaryColorLight: kligtgrey,
          accentColor: kDarkPink,
        ),
        initialRoute: WelcomeScreen.id,
        routes: {
          WelcomeScreen.id: (context) => WelcomeScreen(),
          SignUpScreen.id: (context) => SignUpScreen(),
          HomePage.id: (context) => HomePage(),
          LevelScreen.id: (context) => LevelScreen(),
          SequencesScreen.id: (context) => SequencesScreen(),
          SequenceScreen.id: (context) => SequenceScreen(),
          ProgressScreen.id: (context) => ProgressScreen(),
          StatisticsScreen.id: (context) => StatisticsScreen(),
          ProfileCard.id: (context) => ProfileCard(),
          AppointmentScreen.id: (ctx) => const AppointmentScreen(),
        },
      ),
    );
  }
}

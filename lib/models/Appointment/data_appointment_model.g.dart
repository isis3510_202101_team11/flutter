// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_appointment_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataAppointmentModel _$DataAppointmentModelFromJson(Map<String, dynamic> json) {
  return DataAppointmentModel(
    appointDate: json['ApointDate'] as String,
    createdDate: json['CreatDate'] as String,
    doctorName: json['DoctorName'] as String,
    modifyDate: json['ModifyDate'] as String,
    state: json['Estate'] as int,
    type: json['Type'] as String,
    id: json['appointmentId'] as String,
  );
}

Map<String, dynamic> _$DataAppointmentModelToJson(
        DataAppointmentModel instance) =>
    <String, dynamic>{
      'ApointDate': instance.appointDate,
      'CreatDate': instance.createdDate,
      'DoctorName': instance.doctorName,
      'Estate': instance.state,
      'ModifyDate': instance.modifyDate,
      'Type': instance.type,
      'appointmentId': instance.id,
    };
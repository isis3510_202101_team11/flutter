import 'package:bloc/bloc.dart';

class CrudBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    // ignore: avoid_print
    print('--> onEvent $event');
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    // ignore: avoid_print
    print('|--> onTransition $transition');
  }

  @override
  void onError(BlocBase base, Object error, StackTrace stackTrace) {
    super.onError(base, error, stackTrace);
    // ignore: avoid_print
    print('* --> onError $error');
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:minihomelab/Controllers/DataBase_Provider.dart';

class userData {
  int id;
  String userId = "";
  int age;
  DateTime birthday;
  String name;
  DateTime lastLogin;
  String profilePick;

  String ProfilepickUrl;

  userData(
      {@required this.userId,
      @required this.age,
      @required this.birthday,
      @required this.name,
      @required this.lastLogin,
      this.profilePick = "",
      this.ProfilepickUrl});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DataBaseProvider.COLUMN_UserID: userId,
      DataBaseProvider.COLUMN_NAME: name,
      DataBaseProvider.COLUMN_BIRTHDAY: birthday.millisecondsSinceEpoch,
      DataBaseProvider.COLUMN_PROFILEPICTURE: profilePick,
      DataBaseProvider.COLUMN_PICURL: ProfilepickUrl,
      DataBaseProvider.COLUMN_LASTLOGIN: lastLogin.millisecondsSinceEpoch,
    };
    if (id != null) {
      map[DataBaseProvider.COLUMN_ID] = id;
    }
    return map;
  }

  userData.fromMap(Map<String, dynamic> map) {
    id = map[DataBaseProvider.COLUMN_ID];
    userId = map[DataBaseProvider.COLUMN_UserID];
    name = map[DataBaseProvider.COLUMN_NAME];
    birthday = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.COLUMN_BIRTHDAY]);
    profilePick = map[DataBaseProvider.COLUMN_PROFILEPICTURE];
    ProfilepickUrl = map[DataBaseProvider.COLUMN_PICURL];
    lastLogin = DateTime.fromMillisecondsSinceEpoch(
        map[DataBaseProvider.COLUMN_LASTLOGIN]);
  }
}

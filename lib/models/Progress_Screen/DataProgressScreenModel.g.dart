// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'DataProgressScreenModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataProgressScreenModel _$DataProgressScreenModelFromJson(
    Map<String, dynamic> json) {
  return DataProgressScreenModel(
    dataRomMonth: (json['dataRomMonth'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, (e as num)?.toDouble()),
            ))
        ?.toList(),
    dataTiempoRWeek: (json['dataTiempoRWeek'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, (e as num)?.toDouble()),
            ))
        ?.toList(),
    dataFuerzaMonth: (json['dataFuerzaMonth'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, (e as num)?.toDouble()),
            ))
        ?.toList(),
    dataFuerzaWeek: (json['dataFuerzaWeek'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, (e as num)?.toDouble()),
            ))
        ?.toList(),
    dataRomWeek: (json['dataRomWeek'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, (e as num)?.toDouble()),
            ))
        ?.toList(),
    dataTiempoRMonth: (json['dataTiempoRMonth'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, (e as num)?.toDouble()),
            ))
        ?.toList(),
    destacadosFuerzaMonth: (json['destacadosFuerzaMonth'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            ))
        ?.toList(),
    destacadosFuerzaWeek: (json['destacadosFuerzaWeek'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            ))
        ?.toList(),
    destacadosRomMonth: (json['destacadosRomMonth'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            ))
        ?.toList(),
    destacadosRomWeek: (json['destacadosRomWeek'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            ))
        ?.toList(),
    destacadosTiempoRMonth: (json['destacadosTiempoRMonth'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            ))
        ?.toList(),
    destacadosTiempoRWeek: (json['destacadosTiempoRWeek'] as List)
        ?.map((e) => (e as Map<String, dynamic>)?.map(
              (k, e) => MapEntry(k, e as String),
            ))
        ?.toList(),
  );
}

Map<String, dynamic> _$DataProgressScreenModelToJson(
        DataProgressScreenModel instance) =>
    <String, dynamic>{
      'dataRomWeek': instance.dataRomWeek,
      'dataRomMonth': instance.dataRomMonth,
      'destacadosRomWeek': instance.destacadosRomWeek,
      'destacadosRomMonth': instance.destacadosRomMonth,
      'dataFuerzaWeek': instance.dataFuerzaWeek,
      'dataFuerzaMonth': instance.dataFuerzaMonth,
      'destacadosFuerzaWeek': instance.destacadosFuerzaWeek,
      'destacadosFuerzaMonth': instance.destacadosFuerzaMonth,
      'dataTiempoRWeek': instance.dataTiempoRWeek,
      'dataTiempoRMonth': instance.dataTiempoRMonth,
      'destacadosTiempoRWeek': instance.destacadosTiempoRWeek,
      'destacadosTiempoRMonth': instance.destacadosTiempoRMonth,
    };
